#ifndef IR_H
#define IR_H

#include <stdbool.h>
#include <llvm-c/Types.h>

typedef struct IrType IrType;

typedef struct
{
    IrType *inner;
    uint length;
} IrArray;

typedef struct IrStructMember IrStructMember;

typedef struct
{
    char *name;
    IrStructMember *members;
    size_t members_length;
} IrStruct;

struct IrType
{
    enum
    {
        IVoid,
        IUntypedInt,
        IInt,
        IUint,
        IFloat,
        IBool,
        IPointer,
        IArray,
        ISlice,
        IStruct,
    } kind;

    union
    {
        uint bytes;
        IrType *inner;
        IrArray array;
        IrStruct struct_type;
    };
};

struct IrStructMember
{
    char *name;
    IrType type;
};


typedef struct
{
    char *name;
    IrType type;
    LLVMValueRef address;
} IrVariable;

typedef struct
{
    char *name;
    IrType type;
    LLVMValueRef value;
} IrParameter;

typedef struct
{
    char *name;
    IrParameter *parameters;
    size_t parameters_length;
    IrType return_type;
    LLVMTypeRef signature;
    LLVMValueRef address;
} IrFunctionPrototype;

typedef struct IrExpression IrExpression;

typedef struct
{
    IrFunctionPrototype *function;
    IrExpression *parameters;
    size_t parameters_length;
} IrFunctionCall;

typedef struct
{
    enum
    {
        IAdd,
        ISubtract,
        IMutliply,
        IDivide,
        IEqualTo,
        ILessThan,
        IGreaterThan,
        ILessOrEqual,
        IGreaterOrEqual,
    } kind;

    IrExpression *left;
    IrExpression *right;
} IrBinary;

typedef struct IrStatement IrStatement;

typedef struct
{
    IrStatement *statements;
    size_t statements_length;
    IrType yield_type;
    bool returns;
} IrBlock;

typedef struct
{
    IrExpression *expression;
    IrType type;
} IrCast;

typedef struct
{
    IrExpression *elements;
    size_t length;
    size_t repeats;
} IrArrayLiteral;

typedef struct
{
    IrType type;
    IrExpression *members;
    size_t members_length;
} IrStructLiteral;

typedef struct
{
    IrExpression *array;
    IrExpression *index;
} IrIndex;

typedef struct
{
    IrExpression *array;
    IrExpression *start;
    IrExpression *end;
} IrSliceIndex;

typedef struct
{
    IrExpression *parent;
    uint member_index;
} IrStructMemberAccess;

typedef struct IrIf IrIf;
typedef struct IrSwitch IrSwitch;

struct IrExpression
{
    enum
    {
        IIntLiteral,
        IFloatLiteral,
        ICharacterLiteral,
        IFunctionCall,
        IVariable,
        IParameter,
        IBinary,
        INegate,
        IReference,
        IDereference,
        IBlock,
        IIfExpression,
        ISwitchExpression,
        ICast,
        IArrayLiteral,
        IStructLiteral,
        IIndex,
        ISliceIndex,
        IStructMemberAccess,
        ISliceLength,
    } kind;

    union
    {
        uint int_literal;
        float float_literal;
        char character_literal;
        IrFunctionCall function_call;
        IrVariable *variable;
        IrParameter *parameter;
        IrBinary binary;
        IrExpression *unary;
        IrBlock block;
        IrIf *if_expression;
        IrSwitch *switch_expression;
        IrCast cast;
        IrArrayLiteral array_literal;
        IrStructLiteral struct_literal;
        IrIndex index;
        IrSliceIndex slice_index;
        IrStructMemberAccess struct_member_access;
        IrExpression *slice_length;
    };

    IrType type;
};

typedef struct
{
    IrVariable *variable;
    IrExpression value;
} IrVariableDefinition;

typedef struct
{
    IrExpression left_hand_side;
    IrExpression value;
} IrAssignment;

typedef struct
{
    bool has_value;
    IrExpression value;
} IrReturn;

typedef struct
{
    IrExpression value;
} IrYield;

struct IrIf
{
    IrExpression condition;
    IrExpression if_branch;
    bool has_else;
    IrExpression else_branch;
    IrType yield_type;
};

typedef struct
{
    IrExpression label;
    IrExpression value;
} IrSwitchBranch;

struct IrSwitch
{
    IrExpression value;
    IrSwitchBranch *branches;
    size_t branches_length;
    IrExpression else_value;
};

typedef struct
{
    IrExpression condition;
    IrBlock body;
} IrWhile;

struct IrStatement
{
    enum
    {
        IVariableDefinition,
        IAssignment,
        IReturn,
        IYield,
        IIfStatement,
        ISwitchStatement,
        IWhile,
        IExpression,
        ISubBlock,
    } kind;

    union
    {
        IrVariableDefinition variable_definition;
        IrAssignment assignment;
        IrReturn return_statement;
        IrYield yield;
        IrIf if_statement;
        IrSwitch switch_statement;
        IrWhile while_statement;
        IrExpression expression;
        IrBlock sub_block;
    };
};

typedef struct
{
    IrFunctionPrototype *prototype;
    IrBlock body;
} IrFunctionDefinition;

typedef struct
{
    IrFunctionDefinition *functions;
    size_t functions_length;
} Ir;

#endif
