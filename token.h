#ifndef TOKEN_H
#define TOKEN_H

#include <sys/types.h>

typedef struct
{
    enum
    {
        TEnd,
        TIdentifier,
        TIntLiteral,
        TFloatLiteral,
        TCharacterLiteral,
        TStringLiteral,
        TFn,
        TStruct,
        TVar,
        TIf,
        TElse,
        TSwitch,
        TCase,
        TWhile,
        TReturn,
        TYield,
        TAs,
        TLeftParen,
        TRightParen,
        TLeftCurly,
        TRightCurly,
        TLeftBracket,
        TRightBracket,
        TPlus,
        TMinus,
        TAsterisk,
        TSlash,
        TAmpersand,
        TEqual,
        TEqualTo,
        TLessThan,
        TGreaterThan,
        TLessOrEqual,
        TGreaterOrEqual,
        TComma,
        TDot,
        TDoubleDot,
        TSemi,
    } kind;

    union
    {
        uint int_literal;
        float float_literal;
        char character_literal;
        char *string;
    };
} Token;

#endif
