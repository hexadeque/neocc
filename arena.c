#include "arena.h"

#include <sys/mman.h>
#include <stdint.h>

struct Arena
{
    size_t pos;
    uint8_t buffer[];
};

const size_t ARENA_SIZE = 1000000000;

Arena *arena_create()
{
    Arena *arena = mmap(NULL, ARENA_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    arena->pos = 0;
    return arena;
}

void *arena_alloc(Arena *arena, size_t size)
{
    void *start = arena->buffer + arena->pos;
    arena->pos += size;
    return start;
}

void arena_destroy(Arena *arena)
{
    munmap(arena, ARENA_SIZE);
}

ArenaScratch arena_scratch_get()
{
    static Arena *arena = NULL;

    if (arena == NULL)
    {
        arena = arena_create();
    }

    return (ArenaScratch) {
        .arena = arena,
        .start = arena->pos,
    };
}

void arena_scratch_release(ArenaScratch scratch)
{
    scratch.arena->pos = scratch.start;
}
