#ifndef LEXER_H
#define LEXER_H

#include <stdio.h>

#include "arena.h"
#include "token.h"

Token *lexer_tokenize(FILE *file, Arena *string_arena, Arena *token_arena);

#endif
