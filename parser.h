#ifndef PARSER_H
#define PARSER_H

#include "ast.h"
#include "token.h"
#include "arena.h"

Ast *parser_parse(Token *tokens, Arena *arena);

#endif
