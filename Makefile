CC=cc
CFLAGS=-Wall -Werror -g `llvm-config --cflags`
LDFLAGS=`llvm-config --libs --cflags --ldflags core analysis bitwriter native`
SRC=$(wildcard *.c)
OBJ=$(SRC:%.c=%.o)
TARGET=neocc

all: $(OBJ)
	$(CC) $^ -o $(TARGET) $(LDFLAGS)

%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	rm $(TARGET) *.o
