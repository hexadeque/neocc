#include "compile.h"

#include "arena.h"
#include "ir.h"

#include <llvm-c/Types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <llvm-c/Core.h>
#include <llvm-c/Analysis.h>
#include <llvm-c/BitWriter.h>
#include <llvm-c/TargetMachine.h>

typedef struct
{
    LLVMBasicBlockRef if_block;
    LLVMValueRef if_value;
    LLVMBasicBlockRef else_block;
    LLVMValueRef else_value;
} IfBranches;

typedef struct
{
    LLVMBasicBlockRef block;
    LLVMValueRef value;
} BlockWithValue;

static LLVMTypeRef compile_type(IrType ir);
static LLVMValueRef compile_expression(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function);
static LLVMValueRef compile_address_of_expression(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function);
static void compile_store_expression(LLVMValueRef address, IrExpression expression, LLVMBuilderRef builder, LLVMValueRef function);
static LLVMValueRef compile_statement(IrStatement ir, LLVMBuilderRef builder, LLVMValueRef function);

static LLVMTypeRef get_pointer_type(LLVMTypeRef inner)
{
    return LLVMPointerType(inner, 0);
}

static LLVMTypeRef compile_struct(IrStruct ir)
{
    ArenaScratch scratch = arena_scratch_get();
    LLVMTypeRef *members = arena_alloc(scratch.arena, ir.members_length * sizeof(LLVMTypeRef));

    for (size_t i = 0; i < ir.members_length; i++)
    {
        members[i] = compile_type(ir.members[i].type);
    }

    LLVMTypeRef type = LLVMStructType(members, ir.members_length, false);

    arena_scratch_release(scratch);

    return type;
}

static LLVMTypeRef get_slice_type(LLVMTypeRef inner)
{
    LLVMTypeRef elements[] = {
        get_pointer_type(inner),
        LLVMInt64Type(),
    };

    return LLVMStructType(elements, 2, false);
}

static LLVMTypeRef compile_type(IrType ir)
{
    switch (ir.kind)
    {
        case IVoid:
            return LLVMVoidType();

        case IUntypedInt:
            fprintf(stderr, "Cannot compile untyped int. Note: This is a bug in the compiler!\n");
            exit(1);

        case IInt:
        case IUint:
            return LLVMIntType(ir.bytes * 8);

        case IFloat:
            switch (ir.bytes)
            {
                case 4:
                    return LLVMFloatType();
                case 8:
                    return LLVMDoubleType();
            }

        case IBool:
            return LLVMInt1Type();

        case IPointer:
            return get_pointer_type(compile_type(*ir.inner));

        case IArray:
            return LLVMArrayType(compile_type(*ir.array.inner), ir.array.length);

        case ISlice:
            return get_slice_type(compile_type(*ir.inner));

        case IStruct:
            return compile_struct(ir.struct_type);
    }

    return NULL;
}

static bool type_is_int(IrType ir)
{
    return ir.kind == IInt || ir.kind == IUint;
}

static LLVMValueRef compile_function_call(IrFunctionCall ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    ArenaScratch scratch = arena_scratch_get();
    LLVMValueRef *parameters = arena_alloc(scratch.arena, ir.parameters_length * sizeof(LLVMValueRef));

    for (size_t i = 0; i < ir.parameters_length; i++)
    {
        parameters[i] = compile_expression(ir.parameters[i], builder, function);
    }

    char *name = ir.function->return_type.kind == IVoid ? "" : ir.function->name;

    LLVMValueRef value = LLVMBuildCall2(builder, ir.function->signature, ir.function->address, parameters, ir.parameters_length, name);

    arena_scratch_release(scratch);

    return value;
}

static LLVMValueRef compile_binary(IrBinary ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef left = compile_expression(*ir.left, builder, function);
    LLVMValueRef right = compile_expression(*ir.right, builder, function);

    switch (ir.left->type.kind)
    {
        case IInt:
        case IUint:
            switch (ir.kind)
            {
                case IAdd:
                    return LLVMBuildAdd(builder, left, right, "add");

                case ISubtract:
                    return LLVMBuildSub(builder, left, right, "subtract");

                case IMutliply:
                    return LLVMBuildMul(builder, left, right, "multiply");

                case IDivide:
                    if (ir.left->type.kind == IInt)
                    {
                        return LLVMBuildSDiv(builder, left, right, "divide");
                    }
                    else
                    {
                        return LLVMBuildUDiv(builder, left, right, "divide");
                    }

                case IEqualTo:
                    return LLVMBuildICmp(builder, LLVMIntEQ, left, right, "equal_to");

                case ILessThan:
                    return LLVMBuildICmp(builder, LLVMIntSLT, left, right, "less_than");

                case IGreaterThan:
                    return LLVMBuildICmp(builder, LLVMIntSGT, left, right, "greater_than");

                case ILessOrEqual:
                    return LLVMBuildICmp(builder, LLVMIntSLE, left, right, "less_or_equal");

                case IGreaterOrEqual:
                    return LLVMBuildICmp(builder, LLVMIntSGE, left, right, "greater_or_equal");
            }

        case IFloat:
            switch (ir.kind)
            {
                case IAdd:
                    return LLVMBuildFAdd(builder, left, right, "add");

                case ISubtract:
                    return LLVMBuildFSub(builder, left, right, "subtract");

                case IMutliply:
                    return LLVMBuildFMul(builder, left, right, "multiply");

                case IDivide:
                    return LLVMBuildFDiv(builder, left, right, "divide");

                case IEqualTo:
                    return LLVMBuildFCmp(builder, LLVMRealOEQ, left, right, "equal_to");

                case ILessThan:
                    return LLVMBuildFCmp(builder, LLVMRealOLT, left, right, "less_than");

                case IGreaterThan:
                    return LLVMBuildFCmp(builder, LLVMRealOGT, left, right, "greater_than");

                case ILessOrEqual:
                    return LLVMBuildFCmp(builder, LLVMRealOLE, left, right, "less_or_equal");

                case IGreaterOrEqual:
                    return LLVMBuildFCmp(builder, LLVMRealOGE, left, right, "greater_or_equal");
            }

        default:
            return NULL;
    }
}

static LLVMValueRef compile_negate(IrExpression inner, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef value = compile_expression(inner, builder, function);

    switch (inner.type.kind)
    {
        case IInt:
            return LLVMBuildNeg(builder, value, "negate");

        case IFloat:
            return LLVMBuildFNeg(builder, value, "negate");

        default:
            return NULL;
    }
}

static LLVMValueRef compile_index(IrIndex ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef array = compile_address_of_expression(*ir.array, builder, function);

    if (ir.array->type.kind == ISlice)
    {
        array = LLVMBuildLoad2(builder, get_pointer_type(compile_type(*ir.array->type.inner)), array, "slice_dereference");
    }

    LLVMValueRef index = compile_expression(*ir.index, builder, function);
    return LLVMBuildGEP2(builder, compile_type(*ir.array->type.inner), array, &index, 1, "index");
}

static LLVMValueRef compile_reference(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    switch (ir.kind)
    {
        case IVariable:
            return ir.variable->address;

        case IIndex:
            return compile_index(ir.index, builder, function);

        default:
            return NULL;
    }
}

static BlockWithValue compile_block(IrBlock ir, LLVMBuilderRef builder, LLVMValueRef function, LLVMBasicBlockRef next, bool terminate, const char* name)
{
    LLVMBasicBlockRef block = LLVMAppendBasicBlock(function, name);
    LLVMPositionBuilderAtEnd(builder, block);

    LLVMValueRef yield_value = NULL;

    for (size_t i = 0; i < ir.statements_length; i++)
    {
        LLVMValueRef yield = compile_statement(ir.statements[i], builder, function);

        if (yield != NULL)
        {
            yield_value = yield;
        }
    }

    if (!ir.returns && terminate)
    {
        if (next != NULL)
        {
            LLVMBuildBr(builder, next);
        }
        else
        {
            LLVMBuildRetVoid(builder);
        }
    }

    return (BlockWithValue) {
        .block = block,
        .value = yield_value,
    };
}

static BlockWithValue compile_branch_expression(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function, LLVMBasicBlockRef next, const char* name)
{
    if (ir.kind == IBlock)
    {
        return compile_block(ir.block, builder, function, next, true, name);
    }

    LLVMBasicBlockRef block = LLVMAppendBasicBlock(function, name);
    LLVMPositionBuilderAtEnd(builder, block);

    LLVMValueRef value = compile_expression(ir, builder, function);

    LLVMBuildBr(builder, next);

    return (BlockWithValue) {
        .block = block,
        .value = value,
    };
}

static IfBranches compile_if(IrIf ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMBasicBlockRef from = LLVMGetInsertBlock(builder);
    LLVMBasicBlockRef next = NULL;

    bool all_return = ir.if_branch.kind == IBlock && ir.if_branch.block.returns && ir.has_else && ir.else_branch.kind == IBlock && ir.else_branch.block.returns;

    if (!all_return)
    {
        next = LLVMAppendBasicBlock(function, "continue");
    }

    BlockWithValue if_branch = compile_branch_expression(ir.if_branch, builder, function, next, "if");
    BlockWithValue else_branch;

    if (ir.has_else)
    {
        else_branch = compile_branch_expression(ir.else_branch, builder, function, next, "else");
    }
    else
    {
        else_branch.block = next;
        else_branch.value = NULL;
    }

    LLVMPositionBuilderAtEnd(builder, from);
    LLVMBuildCondBr(builder, compile_expression(ir.condition, builder, function), if_branch.block, else_branch.block);

    LLVMPositionBuilderAtEnd(builder, next);

    return (IfBranches) {
        .if_block = if_branch.block,
        .if_value = if_branch.value,
        .else_block = else_branch.block,
        .else_value = else_branch.value,
    };
}

static void compile_switch(IrSwitch ir, LLVMBuilderRef builder, LLVMValueRef function, LLVMValueRef *values, LLVMBasicBlockRef *blocks)
{
    bool all_return = true;

    for (size_t i = 0; i < ir.branches_length; i++)
    {
        all_return = all_return && ir.branches[i].value.kind == IBlock && ir.branches[i].value.block.returns;
    }

    LLVMBasicBlockRef from = LLVMGetInsertBlock(builder);
    LLVMBasicBlockRef next = NULL;

    if (!all_return)
    {
        next = LLVMAppendBasicBlock(function, "continue");
    }

    BlockWithValue else_branch = compile_branch_expression(ir.else_value, builder, function, next, "else");

    if (blocks != NULL)
    {
        blocks[ir.branches_length] = else_branch.block;
    }

    if (values != NULL)
    {
        values[ir.branches_length] = else_branch.value;
    }

    LLVMPositionBuilderAtEnd(builder, from);
    LLVMValueRef switch_instruction = LLVMBuildSwitch(builder, compile_expression(ir.value, builder, function), else_branch.block, ir.branches_length);

    LLVMValueRef labels[ir.branches_length];

    for (size_t i = 0; i < ir.branches_length; i++)
    {
        labels[i] = compile_expression(ir.branches[i].label, builder, function);
    }

    for (size_t i = 0; i < ir.branches_length; i++)
    {
        BlockWithValue branch = compile_branch_expression(ir.branches[i].value, builder, function, next, "case");
        LLVMAddCase(switch_instruction, labels[i], branch.block);

        if (blocks != NULL)
        {
            blocks[i] = branch.block;
        }

        if (values != NULL)
        {
            values[i] = branch.value;
        }
    }

    LLVMPositionBuilderAtEnd(builder, next);
}

static LLVMValueRef compile_block_expression(IrBlock ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMBasicBlockRef from = LLVMGetInsertBlock(builder);
    LLVMBasicBlockRef next = LLVMAppendBasicBlock(function, "continue");

    BlockWithValue block = compile_block(ir, builder, function, next, true, "block_expression");

    LLVMPositionBuilderAtEnd(builder, from);
    LLVMBuildBr(builder, block.block);

    LLVMPositionBuilderAtEnd(builder, next);

    return block.value;
}

static LLVMValueRef compile_if_expression(IrIf ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    IfBranches branches = compile_if(ir, builder, function);

    LLVMValueRef values[] = { branches.if_value, branches.else_value };
    LLVMBasicBlockRef blocks[] = { branches.if_block, branches.else_block };

    LLVMValueRef phi = LLVMBuildPhi(builder, compile_type(ir.yield_type), "if_phi");
    LLVMAddIncoming(phi, values, blocks, 2);

    return phi;

}

static LLVMValueRef compile_switch_expression(IrSwitch ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef values[ir.branches_length + 1];
    LLVMBasicBlockRef blocks[ir.branches_length + 1];

    compile_switch(ir, builder, function, values, blocks);

    LLVMValueRef phi = LLVMBuildPhi(builder, compile_type(ir.else_value.type), "switch_phi");
    LLVMAddIncoming(phi, values, blocks, ir.branches_length + 1);

    return phi;
}

static LLVMValueRef compile_cast(IrCast ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    if (ir.type.kind == IFloat && ir.expression->type.kind == IInt)
    {
        return LLVMBuildSIToFP(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
    }

    if (ir.type.kind == IFloat && ir.expression->type.kind == IUint)
    {
        return LLVMBuildUIToFP(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
    }

    if (ir.type.kind == IInt && ir.expression->type.kind == IFloat)
    {
        return LLVMBuildFPToSI(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
    }

    if (ir.type.kind == IUint && ir.expression->type.kind == IFloat)
    {
        return LLVMBuildFPToUI(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
    }

    if (type_is_int(ir.type) && type_is_int(ir.expression->type))
    {
        return LLVMBuildIntCast(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
    }

    if (ir.type.kind == IFloat && ir.expression->type.kind == IFloat)
    {
        if (ir.type.bytes > ir.expression->type.bytes)
        {
            return LLVMBuildFPExt(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
        }
        else
        {
            return LLVMBuildFPTrunc(builder, compile_expression(*ir.expression, builder, function), compile_type(ir.type), "cast");
        }
    }

    return NULL;
}

static void compile_array_literal_population(IrArrayLiteral ir, LLVMValueRef array, LLVMTypeRef type, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef elements[ir.length];

    for (size_t i = 0; i < ir.length; i++)
    {
        elements[i] = compile_expression(ir.elements[i], builder, function);
    }

    for (size_t i = 0; i < ir.length * ir.repeats; i++)
    {
        LLVMValueRef index = LLVMConstInt(LLVMInt64Type(), i, false);
        LLVMValueRef element = LLVMBuildGEP2(builder, type, array, &index, 1, "element");
        LLVMBuildStore(builder, elements[i % ir.length], element);
    }
}

static void compile_struct_literal_population(IrStructLiteral ir, LLVMValueRef address, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMTypeRef type = compile_type(ir.type);

    for (size_t i = 0; i < ir.members_length; i++)
    {
        LLVMValueRef member = LLVMBuildStructGEP2(builder, type, address, i, "struct_literal_member");
        compile_store_expression(member, ir.members[i], builder, function);
    }
}

static LLVMValueRef compile_array_literal(IrArrayLiteral ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMTypeRef element_type = compile_type(ir.elements[0].type);
    LLVMValueRef address = LLVMBuildArrayAlloca(builder, element_type, LLVMConstInt(LLVMInt32Type(), ir.length * ir.repeats, false), "array_literal");
    compile_array_literal_population(ir, address, element_type, builder, function);

    return address;
}

static LLVMValueRef compile_struct_literal(IrStructLiteral ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMTypeRef type = compile_type(ir.type);

    LLVMValueRef address = LLVMBuildAlloca(builder, type, "struct_literal");
    compile_struct_literal_population(ir, address, builder, function);
    return address;
}

static LLVMValueRef compile_struct_member_access(IrStructMemberAccess ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef indices[] = {
        LLVMConstInt(LLVMInt32Type(), 0, false),
        LLVMConstInt(LLVMInt32Type(), ir.member_index, false),
    };

    return LLVMBuildGEP2(builder, compile_type(ir.parent->type), compile_address_of_expression(*ir.parent, builder, function), indices, 2, "struct_member_access");
}

static LLVMValueRef compile_slice_length(IrExpression slice, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef address = compile_address_of_expression(slice, builder, function);
    LLVMValueRef length_address = LLVMBuildStructGEP2(builder, compile_type(slice.type), address, 1, "slice_length_address");
    return LLVMBuildLoad2(builder, LLVMInt64Type(), length_address, "slice_length");
}

static LLVMValueRef compile_temp_alloc(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef address = LLVMBuildAlloca(builder, compile_type(ir.type), "temp_alloc");
    compile_store_expression(address, ir, builder, function);
    return address;
}

static LLVMValueRef compile_address_of_expression(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    switch (ir.kind)
    {
        case IVariable:
            return ir.variable->address;

        case IDereference:
            if (ir.unary->kind == IVariable)
            {
                return LLVMBuildLoad2(builder, compile_type(ir.unary->type), compile_address_of_expression(*ir.unary, builder, function), "dereference_lhs");
            }

            return compile_address_of_expression(*ir.unary, builder, function);

        case IArrayLiteral:
            return compile_array_literal(ir.array_literal, builder, function);

        case IStructLiteral:
            return compile_struct_literal(ir.struct_literal, builder, function);

        case IIndex:
            return compile_index(ir.index, builder, function);

        case IStructMemberAccess:
            return compile_struct_member_access(ir.struct_member_access, builder, function);

        default:
            return compile_temp_alloc(ir, builder, function);
    }
}

static void compile_store_slice(IrSliceIndex ir, LLVMValueRef address, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef pointer_address = LLVMBuildStructGEP2(builder, get_slice_type(compile_type(*ir.array->type.inner)), address, 0, "slice_pointer");
    LLVMValueRef length_address = LLVMBuildStructGEP2(builder, get_slice_type(compile_type(*ir.array->type.inner)), address, 1, "slice_length");

    LLVMValueRef array = compile_address_of_expression(*ir.array, builder, function);
    LLVMValueRef start = compile_expression(*ir.start, builder, function);

    LLVMValueRef pointer = LLVMBuildGEP2(builder, compile_type(*ir.array->type.inner), array, &start, 1, "slice_offset");
    LLVMValueRef length = LLVMBuildSub(builder, compile_expression(*ir.end, builder, function), start, "slice_get_length");

    LLVMBuildStore(builder, pointer, pointer_address);
    LLVMBuildStore(builder, length, length_address);
}

static void compile_store_expression(LLVMValueRef address, IrExpression expression, LLVMBuilderRef builder, LLVMValueRef function)
{
    switch (expression.kind)
    {
        case IArrayLiteral:
            compile_array_literal_population(expression.array_literal, address, compile_type(*expression.type.array.inner), builder, function);
            break;

        case IStructLiteral:
            compile_struct_literal_population(expression.struct_literal, address, builder, function);
            break;

        case ISliceIndex:
            compile_store_slice(expression.slice_index, address, builder, function);
            break;

        default:
            LLVMBuildStore(builder, compile_expression(expression, builder, function), address);
            break;
    }
}

static LLVMValueRef compile_expression(IrExpression ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    switch (ir.kind)
    {
        case IIntLiteral:
        case ICharacterLiteral:
            return LLVMConstInt(compile_type(ir.type), ir.int_literal, false);

        case IFloatLiteral:
            return LLVMConstReal(compile_type(ir.type), ir.float_literal);

        case IFunctionCall:
            return compile_function_call(ir.function_call, builder, function);

        case IVariable:
            if (ir.type.kind == IArray)
            {
                return ir.variable->address;
            }
            return LLVMBuildLoad2(builder, compile_type(ir.variable->type), ir.variable->address, "load");

        case IParameter:
            return ir.parameter->value;

        case IBinary:
            return compile_binary(ir.binary, builder, function);

        case INegate:
            return compile_negate(*ir.unary, builder, function);

        case IReference:
            return compile_reference(*ir.unary, builder, function);

        case IDereference:
            return LLVMBuildLoad2(builder, compile_type(ir.type), compile_expression(*ir.unary, builder, function), "dereference");

        case IBlock:
            return compile_block_expression(ir.block, builder, function);

        case IIfExpression:
            return compile_if_expression(*ir.if_expression, builder, function);

        case ISwitchExpression:
            return compile_switch_expression(*ir.switch_expression, builder, function);

        case ICast:
            return compile_cast(ir.cast, builder, function);

        case IArrayLiteral:
            return compile_array_literal(ir.array_literal, builder, function);

        case IStructLiteral:
            return LLVMBuildLoad2(builder, compile_type(ir.type), compile_struct_literal(ir.struct_literal, builder, function), "struct_literal_load");

        case IIndex:
            return LLVMBuildLoad2(builder, compile_type(*ir.index.array->type.inner), compile_index(ir.index, builder, function), "load_index");

        case ISliceIndex:
            return LLVMBuildLoad2(builder, compile_type(ir.type), compile_address_of_expression(ir, builder, function), "load_slice");

        case IStructMemberAccess:
            return LLVMBuildLoad2(builder, compile_type(ir.type), compile_struct_member_access(ir.struct_member_access, builder, function), "load_struct_member");

        case ISliceLength:
            return compile_slice_length(*ir.slice_length, builder, function);
    }

    return 0;
}

static void compile_variable_definition(IrVariableDefinition ir, LLVMBuilderRef builder, LLVMValueRef function)
{
        ir.variable->address = LLVMBuildAlloca(builder, compile_type(ir.variable->type), ir.variable->name);
        compile_store_expression(ir.variable->address, ir.value, builder, function);
}

static void compile_assignment(IrAssignment ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    compile_store_expression(compile_address_of_expression(ir.left_hand_side, builder, function), ir.value, builder, function);
}

static void compile_return(IrReturn ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMValueRef value = ir.has_value ? compile_expression(ir.value, builder, function) : NULL;
    LLVMBuildRet(builder, value);
}

static void compile_while(IrWhile ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMBasicBlockRef from = LLVMGetInsertBlock(builder);
    LLVMBasicBlockRef next = LLVMAppendBasicBlock(function, "continue");

    BlockWithValue loop = compile_block(ir.body, builder, function, NULL, false, "while");
    LLVMBuildCondBr(builder, compile_expression(ir.condition, builder, function), loop.block, next);

    LLVMPositionBuilderAtEnd(builder, from);
    LLVMBuildCondBr(builder, compile_expression(ir.condition, builder, function), loop.block, next);

    LLVMPositionBuilderAtEnd(builder, next);
}

static void compile_sub_block(IrBlock ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    LLVMBasicBlockRef from = LLVMGetInsertBlock(builder);
    LLVMBasicBlockRef next = NULL;

    if (!ir.returns)
    {
        next = LLVMAppendBasicBlock(function, "continue");
    }

    BlockWithValue block = compile_block(ir, builder, function, next, true, "sub_block");

    LLVMPositionBuilderAtEnd(builder, from);
    LLVMBuildBr(builder, block.block);

    LLVMPositionBuilderAtEnd(builder, next);
}

static LLVMValueRef compile_statement(IrStatement ir, LLVMBuilderRef builder, LLVMValueRef function)
{
    switch (ir.kind)
    {
        case IVariableDefinition:
            compile_variable_definition(ir.variable_definition, builder, function);
            break;

        case IAssignment:
            compile_assignment(ir.assignment, builder, function);
            break;

        case IReturn:
            compile_return(ir.return_statement, builder, function);
            break;

        case IYield:
            return compile_expression(ir.yield.value, builder, function);

        case IIfStatement:
            compile_if(ir.if_statement, builder, function);
            break;

        case ISwitchStatement:
            compile_switch(ir.switch_statement, builder, function, NULL, NULL);
            break;

        case IWhile:
            compile_while(ir.while_statement, builder, function);
            break;

        case IExpression:
            compile_expression(ir.expression, builder, function);
            break;

        case ISubBlock:
            compile_sub_block(ir.sub_block, builder, function);
            break;
    }

    return NULL;
}

static void compile_function_prototype(IrFunctionPrototype *ir, LLVMModuleRef module, LLVMBuilderRef builder)
{
    ArenaScratch scratch = arena_scratch_get();
    LLVMTypeRef *parameter_types = arena_alloc(scratch.arena, ir->parameters_length * sizeof(LLVMTypeRef));

    for (size_t i = 0; i < ir->parameters_length; i++)
    {
        if (ir->parameters[i].type.kind != IArray)
        {
            parameter_types[i] = compile_type(ir->parameters[i].type);
        }
        else
        {
            parameter_types[i] = get_pointer_type(compile_type(*ir->parameters[i].type.array.inner));
        }
    }

    ir->signature = LLVMFunctionType(compile_type(ir->return_type), parameter_types, ir->parameters_length, false);

    arena_scratch_release(scratch);

    ir->address = LLVMAddFunction(module, ir->name, ir->signature);

    size_t i = 0;
    for (LLVMValueRef parameter = LLVMGetFirstParam(ir->address); parameter != NULL; parameter = LLVMGetNextParam(parameter))
    {
        LLVMSetValueName2(parameter, ir->parameters[i].name, strlen(ir->parameters[i].name));
        ir->parameters[i].value = parameter;
        i++;
    }
}

void compile(Ir *ir, CompileOptions options)
{
    LLVMModuleRef module = LLVMModuleCreateWithName("module");
    LLVMBuilderRef builder = LLVMCreateBuilder();

    for (size_t i = 0; i < ir->functions_length; i++)
    {
        compile_function_prototype(ir->functions[i].prototype, module, builder);
    }

    for (size_t i = 0; i < ir->functions_length; i++)
    {
        if (ir->functions[i].body.statements != NULL)
        {
            IrFunctionDefinition function = ir->functions[i];
            compile_block(function.body, builder, function.prototype->address, NULL, true, "entry");
        }
    }

    char *error = NULL;
    if (LLVMVerifyModule(module, LLVMPrintMessageAction, &error))
    {
        fprintf(stderr, "Note: This is a bug in the compiler!\n");
        LLVMDisposeMessage(error);
        exit(1);
    }

    if (options.emit_llvm)
    {
        printf("Emitting LLVM!!!\n");
        LLVMWriteBitcodeToFile(module, options.output);
        return;
    }

    LLVMInitializeNativeTarget();
    LLVMInitializeNativeAsmPrinter();
    LLVMInitializeNativeAsmParser();

    LLVMTargetRef target;
    char *triple = LLVMGetDefaultTargetTriple();
    if (LLVMGetTargetFromTriple(triple, &target, &error))
    {
        fprintf(stderr, "Error finding target: %s\n", error);
        LLVMDisposeMessage(error);
        exit(1);
    }

    LLVMTargetMachineRef machine = LLVMCreateTargetMachine(target, triple, "generic", "", LLVMCodeGenLevelDefault, LLVMRelocDefault, LLVMCodeModelDefault);

    if (LLVMTargetMachineEmitToFile(machine, module, options.output, LLVMObjectFile, &error))
    {
        fprintf(stderr, "Error writing object file: %s\n", error);
        LLVMDisposeMessage(error);
        exit(1);
    }

    LLVMDisposeBuilder(builder);
    LLVMDisposeModule(module);
}
