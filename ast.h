#ifndef AST_H
#define AST_H

#include <stdbool.h>
#include <sys/types.h>

typedef struct AstExpression AstExpression;
typedef struct AstIf AstIf;
typedef struct AstSwitch AstSwitch;

typedef struct
{
    char *name;
    AstExpression *parameters;
    size_t parameters_length;
} AstFunctionCall;

typedef struct
{
    enum
    {
        AAdd,
        ASubtract,
        AMultiply,
        ADivide,
        AEqualTo,
        ALessThan,
        AGreaterThan,
        ALessOrEqual,
        AGreaterOrEqual,
    } kind;

    AstExpression *left;
    AstExpression *right;
} AstBinary;

typedef struct AstStatement AstStatement;

typedef struct
{
    AstStatement *statements;
    size_t statements_length;
} AstBlock;

typedef struct AstType AstType;

typedef struct
{
    AstType *inner;
    uint length;
} AstArray;

struct AstType
{
    enum
    {
        ABasicType,
        APointer,
        AArray,
        ASlice,
    } kind;

    union
    {
        char *basic_type;
        AstType *inner;
        AstArray array;
    };
};

typedef struct
{
    AstExpression *expression;
    AstType type;
} AstCast;

typedef struct
{
    AstExpression *elements;
    size_t length;
    size_t repeats;
} AstArrayLiteral;

typedef struct
{
    char *name;
    AstExpression *value;
} AstStructLiteralMember;

typedef struct
{
    AstType type;
    AstStructLiteralMember *members;
    size_t members_length;
} AstStructLiteral;

typedef struct
{
    AstExpression *array;
    AstExpression *index;
} AstIndex;

typedef struct
{
    AstExpression *array;
    AstExpression *start;
    AstExpression *end;
} AstSliceIndex;

typedef struct
{
    AstExpression *parent;
    char *member;
} AstDot;

struct AstExpression
{
    enum
    {
        AIntLiteral,
        AFloatLiteral,
        ACharacterLiteral,
        AFunctionCall,
        AVariable,
        ABinary,
        ANegate,
        AReference,
        ADereference,
        ABlock,
        AIfExpression,
        ASwitchExpression,
        ACast,
        AArrayLiteral,
        AStructLiteral,
        AIndex,
        ASliceIndex,
        ADot,
    } kind;

    union
    {
        uint int_literal;
        float float_literal;
        char character_literal;
        AstFunctionCall function_call;
        char *variable;
        AstBinary binary;
        AstExpression *unary;
        AstBlock block;
        AstIf *if_expression;
        AstSwitch *switch_expression;
        AstCast cast;
        AstArrayLiteral array_literal;
        AstStructLiteral struct_literal;
        AstIndex index;
        AstSliceIndex slice_index;
        AstDot dot;
    };
};

typedef struct
{
    char *name;
    AstExpression value;
    AstType type;
    bool infrenced;
} AstVariableDefinition;

typedef struct
{
    AstExpression left_hand_side;
    AstExpression value;
} AstAssignment;

typedef struct
{
    bool has_value;
    AstExpression value;
} AstReturn;

typedef struct
{
    AstExpression value;
} AstYield;

struct AstIf
{
    AstExpression condition;
    AstExpression if_branch;
    bool has_else;
    AstExpression else_branch;
};

typedef struct
{
    AstExpression label;
    AstExpression value;
} AstSwitchBranch;

struct AstSwitch
{
    AstExpression value;
    AstSwitchBranch *branches;
    size_t branches_length;
    AstExpression else_value;

};

typedef struct
{
    AstExpression condition;
    AstBlock body;
} AstWhile;

struct AstStatement
{
    enum
    {
        AVariableDefinition,
        AAssignment,
        AReturn,
        AYield,
        AIfStatement,
        ASwitchStatement,
        AWhile,
        AExpression,
        ASubBlock,
    } kind;

    union
    {
        AstVariableDefinition variable_definition;
        AstAssignment assignment;
        AstReturn return_statement;
        AstYield yield;
        AstIf if_statement;
        AstSwitch switch_statement;
        AstWhile while_statement;
        AstExpression expression;
        AstBlock sub_block;
    };
};

typedef struct
{
    char *name;
    AstType type;
} AstField;

typedef struct
{
    char *name;
    AstField *parameters;
    size_t parameters_length;
    AstBlock body;
    AstType return_type;
} AstFunctionDefinition;

typedef struct
{
    char *name;
    AstField *members;
    size_t members_length;
} AstStructDefinition;

typedef struct
{
    AstFunctionDefinition *functions;
    size_t functions_length;
    AstStructDefinition *structs;
    size_t structs_length;
} Ast;

#endif
