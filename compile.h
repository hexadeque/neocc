#ifndef COMPILE_H
#define COMPILE_H

#include "ir.h"

typedef struct
{
    char *output;
    bool emit_llvm;
} CompileOptions;

void compile(Ir *ir, CompileOptions options);

#endif
