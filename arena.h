#ifndef ARENA_H
#define ARENA_H

#include <stddef.h>

typedef struct Arena Arena;

typedef struct {
    Arena *arena;
    size_t start;
} ArenaScratch;

Arena *arena_create();
void *arena_alloc(Arena *arena, size_t size);
void arena_destroy(Arena *arena);

ArenaScratch arena_scratch_get();
void arena_scratch_release(ArenaScratch scratch);

#endif
