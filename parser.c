#include "parser.h"
#include "ast.h"
#include "token.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const uint PRECIDENCES[] = {
    [AAdd] = 0,
    [ASubtract] = 0,
    [AMultiply] = 1,
    [ADivide] = 1,
    [AEqualTo] = 2,
    [ALessThan] = 2,
    [AGreaterThan] = 2,
    [ALessOrEqual] = 2,
    [AGreaterOrEqual] = 2,
};

static AstType parse_type(Token *tokens, size_t *pos, Arena *arena);
static AstExpression parse_expression(Token *tokens, size_t *pos, Arena *arena);
static AstStatement parse_statement(Token *tokens, size_t *pos, Arena *arena);

static void unexpected(Token token)
{
    fprintf(stderr, "Unexpected token of type %d\n", token.kind);
    exit(1);
}

static void expect(Token token, uint expected_type)
{
    if (token.kind != expected_type)
    {
        unexpected(token);
    }
}

static AstType get_pointer_type(AstType inner, Arena *arena)
{
    AstType *type = arena_alloc(arena, sizeof(AstType));
    *type = inner;

    return (AstType) {
        .kind = APointer,
        .inner = type,
    };
}

static AstType parse_array(Token *tokens, size_t *pos, Arena *arena)
{
    (*pos)++;

    AstType *inner = arena_alloc(arena, sizeof(AstType));
    *inner = parse_type(tokens, pos, arena);

    if (tokens[*pos].kind == TRightBracket)
    {
        (*pos)++;

        return (AstType) {
            .kind = ASlice,
            .inner = inner,
        };
    }

    expect(tokens[*pos], TSemi);
    (*pos)++;

    expect(tokens[*pos], TIntLiteral);
    uint length = tokens[*pos].int_literal;
    (*pos)++;

    expect(tokens[*pos], TRightBracket);
    (*pos)++;

    return (AstType) {
        .kind = AArray,
        .array = (AstArray) {
            .inner = inner,
            .length = length,
        },
    };
}

static AstType parse_type(Token *tokens, size_t *pos, Arena *arena)
{
    switch (tokens[*pos].kind)
    {
        case TIdentifier:
            (*pos)++;
            return (AstType) {
                .kind = ABasicType,
                .basic_type = tokens[*pos - 1].string,
            };

        case TAsterisk:
            (*pos)++;
            return get_pointer_type(parse_type(tokens, pos, arena), arena);

        case TLeftBracket:
            return parse_array(tokens, pos, arena);

        default:
            unexpected(tokens[*pos]);
    }

    return (AstType) {};
}

static AstExpression parse_function_call(Token *tokens, size_t *pos, Arena *arena)
{
    char *name = tokens[*pos].string;
    (*pos) += 2;

    ArenaScratch scratch = arena_scratch_get();
    AstExpression *parameters = arena_alloc(scratch.arena, 0);
    size_t parameters_length;

    for (parameters_length = 0; tokens[*pos].kind != TRightParen; parameters_length++)
    {
        AstExpression *parameter = arena_alloc(scratch.arena, sizeof(AstExpression));
        *parameter = parse_expression(tokens, pos, arena);

        if (tokens[*pos].kind == TComma)
        {
            (*pos)++;
        }
        else
        {
            expect(tokens[*pos], TRightParen);
        }
    }

    (*pos)++;

    AstExpression *moved = arena_alloc(arena, parameters_length * sizeof(AstExpression));
    memcpy(moved, parameters, parameters_length * sizeof(AstExpression));

    arena_scratch_release(scratch);

    return (AstExpression) {
        .kind = AFunctionCall,
        .function_call = (AstFunctionCall) {
            .name = name,
            .parameters = moved,
            .parameters_length = parameters_length,
        },
    };
}

static AstExpression parse_struct_literal(Token *tokens, size_t *pos, Arena *arena)
{
    AstType type = parse_type(tokens, pos, arena);

    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();

    AstStructLiteralMember *members = arena_alloc(scratch.arena, 0);
    size_t length;

    for (length = 0; tokens[*pos].kind != TRightCurly; length++)
    {
        expect(tokens[*pos], TIdentifier);
        char *name = tokens[*pos].string;
        (*pos)++;

        expect(tokens[*pos], TEqual);
        (*pos)++;

        AstExpression *value = arena_alloc(arena, sizeof(AstExpression));
        *value = parse_expression(tokens, pos, arena);

        AstStructLiteralMember *member = arena_alloc(scratch.arena, sizeof(AstStructLiteralMember));
        *member = (AstStructLiteralMember) {
            .name = name,
            .value = value,
        };

        if (tokens[*pos].kind == TComma)
        {
            (*pos)++;
        }
        else
        {
            expect(tokens[*pos], TRightCurly);
        }
    }

    (*pos)++;

    AstStructLiteralMember *moved = arena_alloc(arena, length * sizeof(AstStructLiteralMember));
    memcpy(moved, members, length * sizeof(AstStructLiteralMember));

    arena_scratch_release(scratch);

    return (AstExpression) {
        .kind = AStructLiteral,
        .struct_literal = (AstStructLiteral) {
            .type = type,
            .members = moved,
            .members_length = length,
        },
    };
}

static AstExpression parse_identifier(Token *tokens, size_t *pos, Arena *arena)
{
    char *name = tokens[*pos].string;

    switch (tokens[*pos + 1].kind)
    {
        case TLeftParen:
            return parse_function_call(tokens, pos, arena);

        case TLeftCurly:
            return parse_struct_literal(tokens, pos, arena);

        default:
            (*pos)++;
            return (AstExpression) {
                .kind = AVariable,
                .variable = name,
            };
    }
}

static AstBlock parse_block(Token *tokens, size_t *pos, Arena *arena)
{
    expect(tokens[*pos], TLeftCurly);
    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();

    AstStatement *statements = arena_alloc(scratch.arena, 0);

    size_t length;
    for (length = 0; tokens[*pos].kind != TRightCurly; length++)
    {
        *(AstStatement *) arena_alloc(scratch.arena, sizeof(AstStatement)) = parse_statement(tokens, pos, arena);
    }

    (*pos)++;

    AstStatement *moved = arena_alloc(arena, length * sizeof(AstStatement));

    memcpy(moved, statements, length * sizeof(AstStatement));
    arena_scratch_release(scratch);

    return (AstBlock) {
        .statements = moved,
        .statements_length = length,
    };
}

static AstExpression parse_array_literal(Token *tokens, size_t *pos, Arena *arena)
{
    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();

    AstExpression *elements = arena_alloc(scratch.arena, 0);
    size_t length;

    for (length = 0; tokens[*pos].kind != TRightBracket && tokens[*pos].kind != TSemi; length++)
    {
        AstExpression *element = arena_alloc(scratch.arena, sizeof(AstExpression));
        *element = parse_expression(tokens, pos, arena);

        if (tokens[*pos].kind == TComma)
        {
            (*pos)++;
        }
        else if (tokens[*pos].kind != TSemi)
        {
            expect(tokens[*pos], TRightBracket);
        }
    }

    size_t repeats = 1;
    if (tokens[*pos].kind == TSemi)
    {
        (*pos)++;

        expect(tokens[*pos], TIntLiteral);
        repeats = tokens[*pos].int_literal;
        (*pos)++;

        expect(tokens[*pos], TRightBracket);
    }

    (*pos)++;

    AstExpression *moved = arena_alloc(arena, length * sizeof(AstExpression));
    memcpy(moved, elements, length * sizeof(AstExpression));

    arena_scratch_release(scratch);

    return (AstExpression) {
        .kind = AArrayLiteral,
        .array_literal = (AstArrayLiteral) {
            .elements = moved,
            .length = length,
            .repeats = repeats,
        },
    };
}

static AstArrayLiteral parse_string_literal(char *string, Arena *arena)
{
    size_t length = strlen(string) + 1;
    AstExpression *elements = arena_alloc(arena, length * sizeof(AstExpression));

    for (size_t i = 0; i < length - 1; i++)
    {
        elements[i] = (AstExpression) {
            .kind = ACharacterLiteral,
            .character_literal = string[i],
        };
    }

    elements[length - 1] = (AstExpression) {
        .kind = ACharacterLiteral,
        .character_literal = 0,
    };

    return (AstArrayLiteral) {
        .elements = elements,
        .length = length,
        .repeats = 1,
    };
}

static AstExpression parse_primary(Token *tokens, size_t *pos, Arena *arena)
{
    switch (tokens[*pos].kind)
    {
        case TLeftParen:
            (*pos)++;
            AstExpression inner = parse_expression(tokens, pos, arena);

            expect(tokens[*pos], TRightParen);
            (*pos)++;

            return inner;

        case TLeftBracket:
            return parse_array_literal(tokens, pos, arena);

        case TIntLiteral:
            (*pos)++;

            return (AstExpression) {
                .kind = AIntLiteral,
                .int_literal = tokens[*pos - 1].int_literal,
            };

        case TFloatLiteral:
            (*pos)++;

            return (AstExpression) {
                .kind = AFloatLiteral,
                .float_literal = tokens[*pos - 1].float_literal,
            };

        case TCharacterLiteral:
            (*pos)++;

            return (AstExpression) {
                .kind = ACharacterLiteral,
                .character_literal = tokens[*pos - 1].character_literal,
            };

        case TStringLiteral:
            (*pos)++;
            return (AstExpression) {
                .kind = AArrayLiteral,
                .array_literal = parse_string_literal(tokens[*pos - 1].string, arena),
            };

        case TIdentifier:
            return parse_identifier(tokens, pos, arena);

        case TLeftCurly:
            return (AstExpression) {
                .kind = ABlock,
                .block = parse_block(tokens, pos, arena),
            };

        default:
            unexpected(tokens[*pos]);
            return (AstExpression) {};
    }
}

static AstExpression parse_postfix(Token *tokens, size_t *pos, Arena *arena)
{
    AstExpression expression = parse_primary(tokens, pos, arena);

    while (true)
    {
        if (tokens[*pos].kind == TLeftBracket)
        {
            AstExpression *array = arena_alloc(arena, sizeof(AstExpression));
            *array = expression;

            (*pos)++;

            AstExpression *index = arena_alloc(arena, sizeof(AstExpression));
            *index = parse_expression(tokens, pos, arena);

            if (tokens[*pos].kind != TDoubleDot)
            {
                expect(tokens[*pos], TRightBracket);
                (*pos)++;

                expression = (AstExpression) {
                    .kind = AIndex,
                    .index = (AstIndex) {
                        .array = array,
                        .index = index,
                    },
                };

                continue;
            }

            (*pos)++;

            AstExpression *end = arena_alloc(arena, sizeof(AstExpression));
            *end = parse_expression(tokens, pos, arena);

            expect(tokens[*pos], TRightBracket);
            (*pos)++;

            expression = (AstExpression) {
                .kind = ASliceIndex,
                .slice_index = (AstSliceIndex) {
                    .array = array,
                    .start = index,
                    .end = end,
                },
            };
        }
        else if (tokens[*pos].kind == TDot)
        {
            (*pos)++;

            AstExpression *parent = arena_alloc(arena, sizeof(AstExpression));
            *parent = expression;

            expect(tokens[*pos], TIdentifier);
            char *member = tokens[*pos].string;
            (*pos)++;

            expression = (AstExpression) {
                .kind = ADot,
                .dot = (AstDot) {
                    .parent = parent,
                    .member = member
                },
            };
        }
        else
        {
            break;
        }
    }

    return expression;
}

static AstExpression parse_unary(Token *tokens, size_t *pos, Arena *arena)
{
    uint kind;

    switch (tokens[*pos].kind)
    {
        case TMinus:
            kind = ANegate;
            break;

        case TAmpersand:
            kind = AReference;
            break;

        case TAsterisk:
            kind = ADereference;
            break;

        default:
            kind = 0;
            break;
    }

    AstExpression expression;

    if (kind != 0)
    {
        (*pos)++;

        AstExpression *inner = arena_alloc(arena, sizeof(AstExpression));
        *inner = parse_unary(tokens, pos, arena);

        expression = (AstExpression) {
            .kind = kind,
            .unary = inner,
        };
    }
    else
    {
        expression = parse_postfix(tokens, pos, arena);
    }

    if (tokens[*pos].kind == TAs)
    {
        (*pos)++;

        AstExpression *inner = arena_alloc(arena, sizeof(AstExpression));
        *inner = expression;

        expression = (AstExpression) {
            .kind = ACast,
            .cast = (AstCast) {
                .expression = inner,
                .type = parse_type(tokens, pos, arena),
            },
        };
    }

    return expression;
}

static AstIf parse_if(Token *tokens, size_t *pos, Arena *arena)
{
    (*pos)++;

    expect(tokens[*pos], TLeftParen);
    (*pos)++;

    AstExpression condition = parse_expression(tokens, pos, arena);

    expect(tokens[*pos], TRightParen);
    (*pos)++;

    AstExpression if_branch = parse_expression(tokens, pos, arena);

    bool has_else = tokens[*pos].kind == TElse;
    AstExpression else_branch;

    if (has_else)
    {
        (*pos)++;
        else_branch = parse_expression(tokens, pos, arena);
    }

    return (AstIf) {
        .condition = condition,
        .if_branch = if_branch,
        .has_else = has_else,
        .else_branch = else_branch,
    };
}

static AstSwitch parse_switch(Token *tokens, size_t *pos, Arena *arena)
{
    (*pos)++;

    AstExpression value = parse_expression(tokens, pos, arena);

    expect(tokens[*pos], TLeftCurly);
    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();
    AstSwitchBranch *branches = arena_alloc(scratch.arena, 0);
    size_t branches_length;

    for (branches_length = 0; tokens[*pos].kind != TElse; branches_length++)
    {
        expect(tokens[*pos], TCase);
        (*pos)++;

        expect(tokens[*pos], TLeftParen);
        (*pos)++;

        AstSwitchBranch *branch = arena_alloc(scratch.arena, sizeof(AstSwitchBranch));

        branch->label = parse_expression(tokens, pos, arena);

        expect(tokens[*pos], TRightParen);
        (*pos)++;

        branch->value = parse_expression(tokens, pos, arena);
    }

    AstSwitchBranch *moved = arena_alloc(arena, branches_length * sizeof(AstSwitchBranch));
    memcpy(moved, branches, branches_length * sizeof(AstSwitchBranch));

    arena_scratch_release(scratch);

    (*pos)++;

    AstExpression else_value = parse_expression(tokens, pos, arena);

    expect(tokens[*pos], TRightCurly);
    (*pos)++;

    return (AstSwitch) {
        .value = value,
        .branches = moved,
        .branches_length = branches_length,
        .else_value = else_value,
    };
}

static AstExpression parse_expression(Token *tokens, size_t *pos, Arena *arena)
{
    if (tokens[*pos].kind == TIf)
    {
        AstIf *if_expression = arena_alloc(arena, sizeof(AstIf));
        *if_expression = parse_if(tokens, pos, arena);

        return (AstExpression) {
            .kind = AIfExpression,
            .if_expression = if_expression,
        };
    }

    if (tokens[*pos].kind == TSwitch)
    {
        AstSwitch *switch_expression = arena_alloc(arena, sizeof(AstSwitch));
        *switch_expression = parse_switch(tokens, pos, arena);

        return (AstExpression) {
            .kind = ASwitchExpression,
            .switch_expression = switch_expression,
        };
    }


    AstExpression expression = parse_unary(tokens, pos, arena);

    while (1)
    {
        int type;

        switch (tokens[*pos].kind)
        {
            case TPlus:
                type = AAdd;
                break;

            case TMinus:
                type = ASubtract;
                break;

            case TAsterisk:
                type = AMultiply;
                break;

            case TSlash:
                type = ADivide;
                break;

            case TEqualTo:
                type = AEqualTo;
                break;

            case TLessThan:
                type = ALessThan;
                break;

            case TGreaterThan:
                type = AGreaterThan;
                break;

            case TLessOrEqual:
                type = ALessOrEqual;
                break;

            case TGreaterOrEqual:
                type = AGreaterOrEqual;
                break;

            default:
                type = -1;
                break;
        }

        if (type == -1)
        {
            break;
        }

        (*pos)++;

        AstBinary binary = {
            .kind = type,
            .left = arena_alloc(arena, sizeof(AstExpression)),
            .right = arena_alloc(arena, sizeof(AstExpression)),
        };

        *binary.left = expression;
        *binary.right = parse_unary(tokens, pos, arena);

        expression = (AstExpression) {
            .kind = ABinary,
            .binary = binary,
        };

        AstExpression *previous = binary.left;

        if (previous->kind == ABinary && PRECIDENCES[previous->binary.kind] < PRECIDENCES[binary.kind])
        {
            uint temp = expression.binary.kind;
            expression.binary.kind = previous->binary.kind;
            previous->binary.kind = temp;

            expression.binary.left = previous->binary.left;
            previous->binary.left = previous->binary.right;
            previous->binary.right = expression.binary.right;
            expression.binary.right = previous;
        }
    }

    return expression;
}

static AstStatement parse_statement(Token *tokens, size_t *pos, Arena *arena)
{
    if (tokens[*pos].kind == TVar)
    {
        (*pos)++;

        char *name = tokens[*pos].string;
        expect(tokens[*pos], TIdentifier);

        (*pos)++;

        bool infrenced = tokens[*pos].kind == TEqual;
        AstType type;

        if (!infrenced)
        {
            type = parse_type(tokens, pos, arena);
        }

        expect(tokens[*pos], TEqual);

        (*pos)++;

        AstExpression expression = parse_expression(tokens, pos, arena);
        expect(tokens[*pos], TSemi);

        (*pos)++;

        return (AstStatement) {
            .kind = AVariableDefinition,
            .variable_definition = {
                .name = name,
                .value = expression,
                .type = type,
                .infrenced = infrenced,
            },
        };
    }

    if (tokens[*pos].kind == TReturn)
    {
        (*pos)++;

        if (tokens[*pos].kind == TSemi)
        {
            (*pos)++;

            return (AstStatement) {
                .kind = AReturn,
                .return_statement = (AstReturn) {
                    .has_value = false,
                },
            };
        }

        AstExpression expression = parse_expression(tokens, pos, arena);
        expect(tokens[*pos], TSemi);

        (*pos)++;

        return (AstStatement) {
            .kind = AReturn,
            .return_statement = (AstReturn) {
                .has_value = true,
                .value = expression,
            },
        };
    }

    if (tokens[*pos].kind == TYield)
    {
        (*pos)++;

        AstExpression value = parse_expression(tokens, pos, arena);

        expect(tokens[*pos], TSemi);
        (*pos)++;

        return (AstStatement) {
            .kind = AYield,
            .yield = (AstYield) {
                .value = value,
            },
        };
    }

    if (tokens[*pos].kind == TIf)
    {
        AstIf if_statement = parse_if(tokens, pos, arena);
        if (if_statement.if_branch.kind != ABlock || (if_statement.has_else && if_statement.else_branch.kind != ABlock))
        {
            fprintf(stderr, "If statement branches must be blocks");
            exit(1);
        }

        return (AstStatement) {
            .kind = AIfStatement,
            .if_statement = if_statement,
        };
    }

    if (tokens[*pos].kind == TSwitch)
    {
        AstSwitch switch_statement = parse_switch(tokens, pos, arena);

        for (size_t i = 0; i < switch_statement.branches_length; i++)
        {
            if (switch_statement.branches[i].value.kind != ABlock)
            {
                fprintf(stderr, "Switch statement branches must be blocks");
            }
        }

        return (AstStatement) {
            .kind = ASwitchStatement,
            .switch_statement = switch_statement,
        };
    }

    if (tokens[*pos].kind == TWhile)
    {
        (*pos)++;

        expect(tokens[*pos], TLeftParen);
        (*pos)++;

        AstExpression condition = parse_expression(tokens, pos, arena);

        expect(tokens[*pos], TRightParen);
        (*pos)++;

        return (AstStatement) {
            .kind = AWhile,
            .while_statement = (AstWhile) {
                .condition = condition,
                .body = parse_block(tokens, pos, arena),
            },
        };
    }

    if (tokens[*pos].kind == TLeftCurly)
    {
        return (AstStatement) {
            .kind = ASubBlock,
            .sub_block = parse_block(tokens, pos, arena),
        };
    }

    AstExpression expression = parse_expression(tokens, pos, arena);

    if (tokens[*pos].kind == TEqual)
    {
        (*pos)++;

        AstExpression value = parse_expression(tokens, pos, arena);
        expect(tokens[*pos], TSemi);

        (*pos)++;

        return (AstStatement) {
            .kind = AAssignment,
            .assignment = (AstAssignment) {
                .left_hand_side = expression,
                .value = value,
            },
        };
    }

    expect(tokens[*pos], TSemi);

    (*pos)++;

    return (AstStatement) {
        .kind = AExpression,
        .expression = expression,
    };
}

static AstFunctionDefinition parse_function(Token *tokens, size_t *pos, Arena *arena)
{
    expect(tokens[*pos], TFn);
    (*pos)++;

    expect(tokens[*pos], TIdentifier);
    char *name = tokens[*pos].string;
    (*pos)++;

    expect(tokens[*pos], TLeftParen);
    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();
    AstField *parameters = arena_alloc(scratch.arena, 0);
    size_t parameters_length;

    for (parameters_length = 0; tokens[*pos].kind != TRightParen; parameters_length++)
    {
        AstField *parameter = arena_alloc(scratch.arena, sizeof(AstField));

        expect(tokens[*pos], TIdentifier);
        parameter->name = tokens[*pos].string;
        (*pos)++;

        parameter->type = parse_type(tokens, pos, arena);

        if (tokens[*pos].kind == TComma)
        {
            (*pos)++;
        }
        else
        {
            expect(tokens[*pos], TRightParen);
        }
    }

    (*pos)++;

    AstField *moved = arena_alloc(arena, parameters_length * sizeof(AstField));
    memcpy(moved, parameters, parameters_length * sizeof(AstField));

    arena_scratch_release(scratch);

    AstType return_type = parse_type(tokens, pos, arena);

    AstBlock body;

    if (tokens[*pos].kind == TSemi)
    {
        (*pos)++;

        body = (AstBlock) {
            .statements = NULL,
            .statements_length = 0,
        };
    }
    else
    {
        body = parse_block(tokens, pos, arena);
    }

    AstFunctionDefinition function = {
        .name = name,
        .parameters = moved,
        .parameters_length = parameters_length,
        .body = body,
        .return_type = return_type,
    };

    return function;
}

static AstStructDefinition parse_struct(Token *tokens, size_t *pos, Arena *arena)
{
    expect(tokens[*pos], TStruct);
    (*pos)++;

    expect(tokens[*pos], TIdentifier);
    char *name = tokens[*pos].string;
    (*pos)++;

    expect(tokens[*pos], TLeftCurly);
    (*pos)++;

    ArenaScratch scratch = arena_scratch_get();

    AstField *members = arena_alloc(scratch.arena, 0);
    size_t length;

    for (length = 0; tokens[*pos].kind != TRightCurly; length++)
    {
        AstField *member = arena_alloc(scratch.arena, sizeof(AstField));

        expect(tokens[*pos], TIdentifier);
        member->name = tokens[*pos].string;
        (*pos)++;

        member->type = parse_type(tokens, pos, arena);

        if (tokens[*pos].kind == TComma)
        {
            (*pos)++;
        }
        else
        {
            expect(tokens[*pos], TRightCurly);
        }
    }

    (*pos)++;

    AstField *moved = arena_alloc(arena, length * sizeof(AstField));
    memcpy(moved, members, length * sizeof(AstField));

    arena_scratch_release(scratch);

    return (AstStructDefinition) {
        .name = name,
        .members = moved,
        .members_length = length,
    };
}

Ast *parser_parse(Token *tokens, Arena *arena)
{
    size_t pos = 0;

    ArenaScratch scratch = arena_scratch_get();
    AstFunctionDefinition *functions = arena_alloc(scratch.arena, 0);
    size_t functions_length = 0;

    Arena *struct_arena = arena_create();
    AstStructDefinition *structs = arena_alloc(struct_arena, 0);
    size_t structs_length = 0;

    while (tokens[pos].kind != TEnd)
    {
        if (tokens[pos].kind == TFn)
        {
            *(AstFunctionDefinition *) arena_alloc(scratch.arena, sizeof(AstFunctionDefinition)) = parse_function(tokens, &pos, arena);
            functions_length++;
        }
        else
        {
            *(AstStructDefinition *) arena_alloc(struct_arena, sizeof(AstStructDefinition)) = parse_struct(tokens, &pos, arena);
            structs_length++;
        }
    }

    Ast *ast = arena_alloc(arena, sizeof(Ast));
    *ast = (Ast) {
        .functions = arena_alloc(arena, functions_length * sizeof(AstFunctionDefinition)),
        .functions_length = functions_length,
        .structs = arena_alloc(arena, structs_length * sizeof(AstStructDefinition)),
        .structs_length = structs_length
    };

    memcpy(ast->functions, functions, functions_length * sizeof(AstFunctionDefinition));
    arena_scratch_release(scratch);

    memcpy(ast->structs, structs, structs_length * sizeof(AstStructDefinition));
    arena_destroy(struct_arena);

    return ast;
}
