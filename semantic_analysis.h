#ifndef SEMANTIC_ANALYSIS_H
#define SEMANTIC_ANALYSIS_H

#include "arena.h"
#include "ir.h"
#include "ast.h"

Ir *semantic_analysis_analyze(Ast *ast, Arena *arena);

#endif
