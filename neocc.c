#include "lexer.h"
#include "parser.h"
#include "semantic_analysis.h"
#include "compile.h"

#include <bits/getopt_core.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

static const struct option long_options[] = {
    {"output", required_argument, NULL, 'o'},
    {"emit-llvm", no_argument, NULL, 'e'},
};

int main(int argc, char **argv)
{
    CompileOptions options = {
        .output = NULL,
        .emit_llvm = false,
    };

    char flag = 0;
    while (flag != -1)
    {
        int option_index;
        flag = getopt_long(argc, argv, "o:", long_options, &option_index);

        switch (flag)
        {
            case 'o':
                options.output = optarg;
                break;

            case 'e':
                options.emit_llvm = true;
                break;

            case -1:
                break;

            default:
                printf("c: %c\noptarg: %s\n", flag, optarg);
                exit(1);
                break;
        }
    }

    if (options.output == NULL)
    {
        if (options.emit_llvm)
        {
            options.output = "out.bc";
        }
        else
        {
            options.output = "out.o";
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Must provide at least one source file\n");
        exit(1);
    }

    char **source_filenames = &argv[optind];
    size_t source_files = argc - optind;

    if (source_files != 1)
    {
        fprintf(stderr, "Can only use one source file\n");
        exit(1);
    }

    FILE *file = fopen(source_filenames[0], "r");
    if (file == NULL)
    {
        perror("Error reading file");
        exit(1);
    }

    Arena *arena = arena_create();
    Arena *tokens_arena = arena_create();

    Token *tokens = lexer_tokenize(file, arena, tokens_arena);
    fclose(file);

    Ast *ast = parser_parse(tokens, arena);

    arena_destroy(tokens_arena);

    Ir *ir = semantic_analysis_analyze(ast, arena_create());

    arena_destroy(arena);

    compile(ir, options);

    return 0;
}
