#include "lexer.h"

#include "arena.h"
#include "hashmap.h"
#include "token.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

Token *lexer_tokenize(FILE *file, Arena *string_arena, Arena *token_arena)
{
    ArenaScratch scratch = arena_scratch_get();
    HashMap *keywords = hash_map_create(scratch.arena);

    uint keyword_vals[] = { TFn, TStruct, TVar, TIf, TElse, TSwitch, TCase, TWhile, TReturn, TYield, TAs };
    hash_map_insert(keywords, "fn", &keyword_vals[0]);
    hash_map_insert(keywords, "struct", &keyword_vals[1]);
    hash_map_insert(keywords, "var", &keyword_vals[2]);
    hash_map_insert(keywords, "if", &keyword_vals[3]);
    hash_map_insert(keywords, "else", &keyword_vals[4]);
    hash_map_insert(keywords, "switch", &keyword_vals[5]);
    hash_map_insert(keywords, "case", &keyword_vals[6]);
    hash_map_insert(keywords, "while", &keyword_vals[7]);
    hash_map_insert(keywords, "return", &keyword_vals[8]);
    hash_map_insert(keywords, "yield", &keyword_vals[9]);
    hash_map_insert(keywords, "as", &keyword_vals[10]);

    Token *tokens = arena_alloc(token_arena, 0);

    char c = fgetc(file);

    while (c != EOF)
    {
        if (c == ' ' || c == '\n') 
        {
            c = fgetc(file);
            continue;
        }

        if (isalpha(c) || c == '_')
        {
            ArenaScratch scratch = arena_scratch_get();

            char *value = arena_alloc(scratch.arena, 0);

            size_t length;
            for (length = 1; isalnum(c) || c == '_'; length++)
            {
                *(char *) arena_alloc(scratch.arena, 1) = c;
                c = fgetc(file);
            }

            *(char *) arena_alloc(scratch.arena, 1) = 0;

            uint *keyword = hash_map_lookup(keywords, value);

            if (keyword != NULL)
            {
                *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                    .kind = *keyword,
                };

                arena_scratch_release(scratch);

                continue;
            }

            char *moved = arena_alloc(string_arena, length);
            memcpy(moved, value, length);

            arena_scratch_release(scratch);

            *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                .kind = TIdentifier,
                .string = moved,
            };

            continue;
        }

        if (isdigit(c))
        {
            uint value = 0;

            for (; isdigit(c); c = fgetc(file))
            {
                value *= 10;
                value += c - '0';
            }

            char next = fgetc(file);
            ungetc(next, file);

            if (c == '.' && next != '.')
            {
                float decimal = 0.0;

                for (c = fgetc(file); isdigit(c); c = fgetc(file))
                {
                    decimal += c - '0';
                    decimal /= 10;
                }

                *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                    .kind = TFloatLiteral,
                    .float_literal = value + decimal,
                };
            }
            else
            {
                *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                    .kind = TIntLiteral,
                    .int_literal = value,
                };
            }

            continue;
        }

        if (c == '"')
        {
            c = fgetc(file);

            char *value = arena_alloc(string_arena, 0);

            while (c != '"')
            {
                *(char *) arena_alloc(string_arena, 1) = c;
                c = fgetc(file);
            }

            *(char *) arena_alloc(string_arena, 1) = 0;

            c = fgetc(file);

            *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                .kind = TStringLiteral,
                .string = value,
            };

            continue;
        }

        if (c == '\'')
        {
            char character = fgetc(file);

            if (character == '\'')
            {
                fprintf(stderr, "Unexpected \'\n");
                exit(1);
            }

            if (fgetc(file) != '\'')
            {
                fprintf(stderr, "Unterminated character literal\n");
                exit(1);
            }

            c = fgetc(file);

            *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
                .kind = TCharacterLiteral,
                .character_literal = character,
            };

            continue;
        }

        unsigned int kind;

        switch (c)
        {
            case '(':
                kind = TLeftParen;
                break;

            case ')':
                kind = TRightParen;
                break;

            case '{':
                kind = TLeftCurly;
                break;

            case '}':
                kind = TRightCurly;
                break;

            case '[':
                kind = TLeftBracket;
                break;

            case ']':
                kind = TRightBracket;
                break;

            case '+':
                kind = TPlus;
                break;

            case '-':
                kind = TMinus;
                break;

            case '*':
                kind = TAsterisk;
                break;

            case '/':
                c = fgetc(file);
                if (c == '/')
                {
                    while (c != '\n')
                    {
                        c = fgetc(file);
                    }

                    continue;
                }

                ungetc(c, file);
                kind = TSlash;
                break;

            case '&':
                kind = TAmpersand;
                break;

            case '<':
                c = fgetc(file);
                if (c == '=')
                {
                    kind = TLessOrEqual;
                    break;
                }

                ungetc(c, file);
                kind = TLessThan;
                break;

            case '>':
                c = fgetc(file);
                if (c == '=')
                {
                    kind = TGreaterOrEqual;
                    break;
                }

                ungetc(c, file);
                kind = TGreaterThan;
                break;

            case '=':
                c = fgetc(file);
                if (c == '=')
                {
                    kind = TEqualTo;
                    break;
                }

                ungetc(c, file);
                kind = TEqual;
                break;

            case ',':
                kind = TComma;
                break;

            case '.':
                c = fgetc(file);
                if (c == '.')
                {
                    kind = TDoubleDot;
                    break;
                }

                ungetc(c, file);
                kind = TDot;
                break;

            case ';':
                kind = TSemi;
                break;

            default:
                fprintf(stderr, "Unexpected character '%c'\n", c);
                exit(1);
        }

        *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
            .kind = kind,
        };

        c = fgetc(file);
        continue;
    }

    arena_scratch_release(scratch);

    *(Token *) arena_alloc(token_arena, sizeof(Token)) = (Token) {
        .kind = TEnd,
    };

    return tokens;
}
