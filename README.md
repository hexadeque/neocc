# Neo-C
This project is a compile for my own language called Neo-C. This name was chosen because the dream is to eventually have a language that is like a modern version of C.
As if now, the language is very simple and lacking most features as I am still working on it.

## The language
The following code sample shows the syntax and all the features currently in the language.
```rust
// Import functions from libc
fn strndup(str *U8, size U64) *U8;
fn puts(str *U8) I32;

// Struct definition
struct Person
{
    name [U8],
    age I64,
}

// Entry point
fn main() Void
{
    // Variables and type inference
    var x I64 = 1;  // Explicit type
    var y = 2;      // Implicit type

    // Types are I8, I16, I32, I64,
    // U8, U16, U32, U64,
    // F32, F64, Bool, and Void

    // Pointers (are pretty much like C)
    var p_x *I64 = &x;  // Asterisk comes before type;
    var z = *p_x;         // z == 1
    *p_x = 2;           // x is now 2

    // Ifs and Whiles (are just like C)
    if (x == 2)
    {
        // Condition is true
    }
    else
    {
        // Condition is false
    }

    while (x < 10)
    {
        // loop iteration
        x = x + 1;
    }

    // If expressions replace the ternary operator
    var a = 1;
    var b = 2;
    var max = if (a > b) a else b;

    // Block expressions
    var result = {
        // Some operations

        yield 42; // Like return but only for the current block
    }; // result is now 42

    result = if (x == 1) // Can be used in if statements
    {
        yield 12;
    }
    else
    {
        yield 24;
    };

    // Switch statements
    switch (x)
    {
        case (1)
        {
            // Runs if x == 1
        }

        case (2)
        {
            // Runs if x == 2
        }

        else
        {
            // Runs if all cases fail
            // Else branch is not optional and must come last
        }
    }

    // Switch expressions
    y = switch (x)
    {
        case (1) 12 // y will be 12 if x == 1
        case (2) 24

        case (3)
        { // Block expressions and yield word here too
            yield 42;
        }

        else 4
    };

    // Arrays
    var numbers [I64; 3] = [1, 2, 3]; // Array type is [type; length], arrays are enclosed in square brackets
    var sequence = [1, 2, 3, 4; 32]; // The sequence 1,2,3,4 repeated 32 times

    var fourth = sequence[3]; // Indexing like C (U64 is required for index)

    // Slices
    var part_of_numbers = numbers[3..7]; // Gets a refrence to the array `numbers` (defined above) on the interval [3,7)
    part_of_numbers[2] = 42; // Updates the corresponding value in `numbers`
    var another_slice [I64] = part_of_numbers[1..3]; // Type is like arrays but without the "; length", you can also slice other slices

    // Structs
    var name = "John";

    var person = Person { // Struct initialization
        name = name[0..name.len],
        age = 31,
    };

    person.age = 32; // John got older!

    // Functions
    var sum = add(3.0, 4.0); // add defined below

    // Hello World
    var message = "Hello, World"; // Strings are not null terminated, so strndup is used to make a null terminated string
    var terminated = strndup(&message[0], 13); // Address of first element to get a pointer to the array for C

    var n = puts(terminated);
}

fn add(x F32, y F32) F32
{
    return x + y;
}
```

## Compiling & running
Dependencies:
- llvm

To compile, simply run:
```
make
```
Then to use it to compile a Neo-C source file to a binary
```
./neocc source_code.nc
cc out.o
```
