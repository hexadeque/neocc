#include "hashmap.h"

#include <string.h>
#include <strings.h>

#include "arena.h"

#define SIZE 64

typedef struct Entry Entry;
struct Entry
{
    const char *key;
    void *value;
};

struct HashMap
{
    Entry elements[SIZE];
};

static size_t hash(const char *key)
{
    // djb2 hashing algorithm
    size_t hash = 5381;

    for (const char *c = key; *c != 0; c++)
    {
        hash = (hash << 5) + hash + *c;
    }

    return hash % SIZE;
}

HashMap *hash_map_create(Arena *arena)
{
    HashMap *hash_map = arena_alloc(arena, sizeof(HashMap));
    memset(hash_map->elements, 0, sizeof(hash_map->elements));

    return hash_map;
}

bool hash_map_insert(HashMap *hash_map, const char *key, void *value)
{
    size_t index = hash(key);

    if (hash_map_lookup(hash_map, key) != NULL)
    {
        return false;
    }

    for (size_t i = index; i < index + SIZE; i++) {
        Entry *entry = &hash_map->elements[i % SIZE];

        if (entry->key == NULL)
        {
            entry->key = key;
            entry->value = value;
            return true;
        }
    }

    return false;
}

void *hash_map_lookup(HashMap *hash_map, const char *key)
{
    size_t index = hash(key);

    for (size_t i = index; i < index + SIZE; i++)
    {
        Entry entry = hash_map->elements[i % SIZE];

        if (entry.key != NULL && strcmp(entry.key, key) == 0)
        {
            return entry.value;
        }
    }

    return NULL;
}

HashMap *hash_map_copy(Arena *arena, HashMap *original)
{
    HashMap *copy = arena_alloc(arena, sizeof(HashMap));
    memcpy(copy, original, sizeof(HashMap));

    return copy;
}
