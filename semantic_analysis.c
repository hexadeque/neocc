#include "semantic_analysis.h"

#include "arena.h"
#include "ast.h"
#include "hashmap.h"
#include "ir.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    enum
    {
        SymType,
        SymFunction,
        SymLocal,
        SymParameter,
    } kind;

    union
    {
        IrType type;
        IrFunctionPrototype *function;
        IrVariable *local;
        IrParameter *parameter;
    };
} Symbol;

static IrExpression analyze_expression(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype);
static IrBlock analyze_block(AstBlock ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype);

static char *move_string(char *string, Arena *arena)
{
    size_t length = strlen(string) + 1;
    char *moved = arena_alloc(arena, length);
    memcpy(moved, string, length);

    return moved;
}

static bool statement_returns(IrStatement statement)
{
    switch (statement.kind)
    {
        case IReturn:
            return true;

        case ISubBlock:
            return statement.sub_block.returns;

        case IIfStatement:
            return statement.if_statement.has_else && statement.if_statement.if_branch.kind == IBlock && statement.if_statement.if_branch.block.returns && statement.if_statement.else_branch.kind == IBlock && statement.if_statement.else_branch.block.returns;

        case ISwitchStatement:
            for (size_t i = 0; i < statement.switch_statement.branches_length; i++)
            {
                IrExpression value = statement.switch_statement.branches[i].value;

                if (value.kind != IBlock || !value.block.returns)
                {
                    return false;
                }
            }
            return true;

        default:
            return false;
    }
}

static bool statement_terminates(IrStatement ir)
{
    if (statement_returns(ir))
    {
        return true;
    }

    return ir.kind == IYield;
}

static bool types_equal(IrType a, IrType b)
{
    if (a.kind != b.kind)
    {
        return false;
    }

    switch (a.kind)
    {
        case IPointer:
            return types_equal(*a.inner, *b.inner);

        case IInt:
        case IUint:
        case IFloat:
            return a.bytes == b.bytes;

        case IArray:
            return types_equal(*a.array.inner, *b.array.inner) && a.array.length == b.array.length;

        default:
            return true;
    }
}

static bool type_is_int(IrType type)
{
    return type.kind == IInt || type.kind == IUint || type.kind == IUntypedInt;
}

static bool coerce_value(IrExpression *value, IrType target_type)
{
    if (value->type.kind == IUntypedInt && type_is_int(target_type))
    {
        value->type = target_type;

        switch (value->kind)
        {
            case IBinary:
                coerce_value(value->binary.left, target_type);
                coerce_value(value->binary.right, target_type);
                break;

            case INegate:
                coerce_value(value->unary, target_type);
                break;

            case IIfExpression:
                coerce_value(&value->if_expression->if_branch, target_type);
                coerce_value(&value->if_expression->else_branch, target_type);
                break;

            case ISwitchExpression:
                for (size_t i = 0; i < value->switch_expression->branches_length; i++)
                {
                    coerce_value(&value->switch_expression->branches[i].value, target_type);
                }

                coerce_value(&value->switch_expression->else_value, target_type);

                break;

            default:
                break;
        }
    }

    if (value->kind == IArrayLiteral && target_type.kind == IArray)
    {
        value->type = target_type;

        for (size_t i = 0; i < value->array_literal.length; i++)
        {
            coerce_value(&value->array_literal.elements[i], *target_type.array.inner);
        }
    }

    return types_equal(value->type, target_type);
}

static bool coerce_values_to_common_type(IrExpression *a, IrExpression *b)
{
    if (a->type.kind == IUntypedInt)
    {
        return coerce_value(a, b->type);
    }
    else
    {
        return coerce_value(b, a->type);
    }
}

static IrType get_basic_type(uint kind)
{
    return (IrType) {
        .kind = kind,
    };
}

static IrType get_numeric_type(uint kind, uint bytes)
{
    return (IrType) {
        .kind = kind,
        .bytes = bytes,
    };
}

static IrType get_pointer_type(IrType inner, Arena *arena)
{
    IrType *type = arena_alloc(arena, sizeof(IrType));
    *type = inner;

    return (IrType) {
        .kind = IPointer,
        .inner = type,
    };
}

static IrType get_array_type(IrType inner, uint length, Arena *arena)
{
    IrType *type = arena_alloc(arena, sizeof(IrType));
    *type = inner;

    return (IrType) {
        .kind = IArray,
        .array = (IrArray) {
            .inner = type,
            .length = length,
        },
    };
}

static IrType get_slice_type(IrType inner, Arena *arena)
{
    IrType *type = arena_alloc(arena, sizeof(IrType));
    *type = inner;

    return (IrType) {
        .kind = ISlice,
        .inner = type,
    };
}

static IrType type_to_typed(IrType type)
{
    switch (type.kind)
    {
        case IUntypedInt:
            return get_numeric_type(IInt, 8);

        case IArray:
            *type.inner = type_to_typed(*type.inner);
            return type;

        default:
            return type;
    }
}

static void make_typed(IrExpression *value)
{
    coerce_value(value, type_to_typed(value->type));
}

static IrType analyze_type(AstType ast, Arena *arena, HashMap *symbols)
{
    if (ast.kind == APointer)
    {
        return get_pointer_type(analyze_type(*ast.inner, arena, symbols), arena);
    }

    if (ast.kind == AArray)
    {
        return get_array_type(analyze_type(*ast.array.inner, arena, symbols), ast.array.length, arena);
    }

    if (ast.kind == ASlice)
    {
        IrType *inner = arena_alloc(arena, sizeof(IrType));
        *inner = analyze_type(*ast.inner, arena, symbols);

        return get_slice_type(analyze_type(*ast.inner, arena, symbols), arena);
    }

    Symbol *symbol = hash_map_lookup(symbols, ast.basic_type);

    if (symbol == NULL || symbol->kind != SymType)
    {
        fprintf(stderr, "Cannot use nonexisent type '%s'\n", ast.basic_type);
        exit(1);
    }

    return symbol->type;
}

static IrExpression analyze_function_call(AstFunctionCall ast, Arena *arena, HashMap *symbols)
{
    Symbol *symbol = hash_map_lookup(symbols, ast.name);

    if (symbol == NULL || symbol->kind != SymFunction)
    {
        fprintf(stderr, "Cannot call nonexistent function '%s'\n", ast.name);
        exit(1);
    }

    IrFunctionPrototype *prototype = symbol->function;

    if (ast.parameters_length != prototype->parameters_length)
    {
        fprintf(stderr, "Cannot pass %lu parameters to function '%s' which requires %lu parameters\n", ast.parameters_length, ast.name, prototype->parameters_length);
        exit(1);
    }

    IrExpression *parameters = arena_alloc(arena, ast.parameters_length * sizeof(IrExpression));

    for (size_t i = 0; i < ast.parameters_length; i++)
    {
        parameters[i] = analyze_expression(ast.parameters[i], arena, symbols, *prototype);

        if (!coerce_value(&parameters[i], prototype->parameters[i].type))
        {
            fprintf(stderr, "Invalid type passed for parameter '%s' of function '%s'\n", prototype->parameters[i].name, prototype->name);
            exit(1);
        }
    }

    return (IrExpression) {
        .kind = IFunctionCall,
        .function_call = (IrFunctionCall) {
            .function = prototype,
            .parameters = parameters,
            .parameters_length = prototype->parameters_length,
        },
        .type = prototype->return_type,
    };
}

static IrExpression analyze_variable_call(char *name, HashMap *symbols)
{
    Symbol *symbol = hash_map_lookup(symbols, name);

    switch (symbol->kind)
    {
        case SymLocal:
            return (IrExpression) {
                .kind = IVariable,
                .variable = symbol->local,
                .type = symbol->local->type,
            };

        case SymParameter:
            return (IrExpression) {
                .kind = IParameter,
                .parameter = symbol->parameter,
                .type = symbol->parameter->type,
            };

        default:
            fprintf(stderr, "Cannot use nonexistent variable '%s'\n", name);
            exit(1);
    }
}

static IrExpression analyze_binary(AstBinary ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *left = arena_alloc(arena, sizeof(IrExpression));
    *left = analyze_expression(*ast.left, arena, symbols, prototype);

    IrExpression *right = arena_alloc(arena, sizeof(IrExpression));
    *right = analyze_expression(*ast.right, arena, symbols, prototype);

    if (!coerce_values_to_common_type(left, right))
    {
        fprintf(stderr, "Cannot use two values of differing types in a binary expression\n");
        exit(1);
    }

    if (left->type.kind != IInt && left->type.kind != IUint && left->type.kind != IUntypedInt && left->type.kind != IFloat)
    {
        fprintf(stderr, "Can only use numeric types in binary expression\n");
        exit(1);
    }

    IrType type = left->type;

    if (ast.kind == AEqualTo || ast.kind == ALessThan || ast.kind == AGreaterThan || ast.kind == ALessOrEqual || ast.kind == AGreaterOrEqual)
    {
        make_typed(left);
        make_typed(right);
        type = get_basic_type(IBool);
    }

    return (IrExpression) {
        .kind = IBinary,
        .binary = (IrBinary) {
            .kind = ast.kind,
            .left = left,
            .right = right,
        },
        .type = type,
    };
}

static IrExpression analyze_negate(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *inner = arena_alloc(arena, sizeof(IrExpression));
    *inner = analyze_expression(ast, arena, symbols, prototype);

    if (inner->type.kind != IInt && inner->type.kind != IUntypedInt && inner->type.kind != IFloat)
    {
        fprintf(stderr, "Invalid type used in negation\n");
        exit(1);
    }

    return (IrExpression)
    {
        .kind = INegate,
        .unary = inner,
        .type = inner->type,
    };
}

static IrExpression analyze_reference(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *inner = arena_alloc(arena, sizeof(IrExpression));
    *inner = analyze_expression(ast, arena, symbols, prototype);

    if (inner->kind != IVariable && inner->kind != IIndex)
    {
        fprintf(stderr, "Can only reference variables\n");
        exit(1);
    }

    return (IrExpression) {
        .kind = IReference,
        .unary = inner,
        .type = get_pointer_type(inner->type, arena),
    };
}

static IrExpression analyze_dereference(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *inner = arena_alloc(arena, sizeof(IrExpression));
    *inner = analyze_expression(ast, arena, symbols, prototype);

    if (inner->type.kind != IPointer)
    {
        fprintf(stderr, "Can only dereference pointer types\n");
        exit(1);
    }

    return (IrExpression) {
        .kind = IDereference,
        .unary = inner,
        .type = *inner->type.inner,
    };
}

static IrExpression analyze_block_expression(AstBlock ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrBlock block = analyze_block(ast, arena, symbols, prototype);

    return (IrExpression) {
        .kind = IBlock,
        .block = block,
        .type = block.yield_type,
    };
}

static IrIf analyze_if(AstIf ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression condition = analyze_expression(ast.condition, arena, symbols, prototype);

    if (condition.type.kind != IBool)
    {
        fprintf(stderr, "Must use value of type bool for if condition\n");
        exit(1);
    }

    IrExpression if_branch = analyze_expression(ast.if_branch, arena, symbols, prototype);

    if (!ast.has_else && if_branch.type.kind != IVoid)
    {
        fprintf(stderr, "If cannot yield value without else branch\n");
    }

    IrExpression else_branch;

    if (ast.has_else)
    {
        else_branch = analyze_expression(ast.else_branch, arena, symbols, prototype);

        if (!coerce_values_to_common_type(&if_branch, &else_branch))
        {
            fprintf(stderr, "Cannot yield different types from if and else branches\n");
            exit(1);
        }
    }

    return (IrIf) {
        .condition = condition,
        .if_branch = if_branch,
        .has_else = ast.has_else,
        .else_branch = else_branch,
        .yield_type = if_branch.type,
    };
}

static IrExpression analyze_if_expression(AstIf ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrIf *if_expression = arena_alloc(arena, sizeof(IrIf));
    *if_expression = analyze_if(ast, arena, symbols, prototype);

    return (IrExpression) {
        .kind = IIfExpression,
        .if_expression = if_expression,
        .type = if_expression->yield_type,
    };
}

static IrSwitch analyze_switch(AstSwitch ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression value = analyze_expression(ast.value, arena, symbols, prototype);
    make_typed(&value);

    if (value.type.kind != IInt && value.type.kind != IUint)
    {
        fprintf(stderr, "Can only switch on integer types\n");
        exit(1);
    }

    IrExpression else_value = analyze_expression(ast.else_value, arena, symbols, prototype);
    make_typed(&else_value);

    IrSwitchBranch *branches = arena_alloc(arena, ast.branches_length * sizeof(IrSwitchBranch));

    for (size_t i = 0; i < ast.branches_length; i++)
    {
        branches[i].label = analyze_expression(ast.branches[i].label, arena, symbols, prototype);

        if (!coerce_value(&branches[i].label, value.type))
        {
            fprintf(stderr, "Switch cases must match the type of the switched value\n");
            exit(1);
        }

        branches[i].value = analyze_expression(ast.branches[i].value, arena, symbols, prototype);

        if (!coerce_values_to_common_type(&branches[i].value, &else_value))
        {
            fprintf(stderr, "Types of switch branches must match\n");
            exit(1);
        }
    }

    return (IrSwitch) {
        .value = value,
        .branches = branches,
        .branches_length = ast.branches_length,
        .else_value = else_value,
    };
}

static IrExpression analyze_switch_expression(AstSwitch ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrSwitch *switch_expression = arena_alloc(arena, sizeof(IrSwitch));
    *switch_expression = analyze_switch(ast, arena, symbols, prototype);

    return (IrExpression) {
        .kind = ISwitchExpression,
        .switch_expression = switch_expression,
        .type = switch_expression->else_value.type,
    };
}

static IrExpression analyze_cast(AstCast ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression expression = analyze_expression(*ast.expression, arena, symbols, prototype);

    IrType type = analyze_type(ast.type, arena, symbols);

    if (coerce_value(&expression, type))
    {
        return expression;
    }

    if (type.kind == IVoid || type.kind == IPointer || expression.type.kind == IVoid || expression.type.kind == IPointer)
    {
        fprintf(stderr, "Invalid type to cast\n");
        exit(1);
    }

    IrExpression *inner = arena_alloc(arena, sizeof(IrExpression));
    *inner = expression;

    return (IrExpression) {
        .kind = ICast,
        .cast = (IrCast) {
            .expression = inner,
            .type = type,
        },
        .type = type,
    };
}

static IrExpression analyze_array_literal(AstArrayLiteral ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    if (ast.repeats == 0)
    {
        fprintf(stderr, "Array literal repeats must be at least one\n");
        exit(1);
    }

    IrExpression *elements = arena_alloc(arena, ast.length * sizeof(IrExpression));

    for (size_t i = 0; i < ast.length; i++)
    {
        elements[i] = analyze_expression(ast.elements[i], arena, symbols, prototype);

        if (!coerce_values_to_common_type(&elements[i], &elements[0]))
        {
            fprintf(stderr, "Inconsitent types in array\n");
            exit(1);
        }
    }

    return (IrExpression) {
        .kind = IArrayLiteral,
        .array_literal = (IrArrayLiteral) {
            .elements = elements,
            .length = ast.length,
            .repeats = ast.repeats,
        },
        .type = get_array_type(elements[0].type, ast.length * ast.repeats, arena),
    };
}

static IrExpression analyze_struct_literal(AstStructLiteral ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrType type = analyze_type(ast.type, arena, symbols);

    if (type.kind != IStruct)
    {
        fprintf(stderr, "Cannot make struct of non-struct type\n");
        exit(1);
    }

    if (ast.members_length != type.struct_type.members_length)
    {
        fprintf(stderr, "Wrong amount of members for initialization of struct of type '%s'\n", type.struct_type.name);
        exit(1);
    }

    IrExpression *values = arena_alloc(arena, ast.members_length * sizeof(IrExpression));

    for (size_t i = 0; i < ast.members_length; i++)
    {
        for (size_t j = 0; j < ast.members_length; j++)
        {
            if (strcmp(type.struct_type.members[i].name, ast.members[j].name) == 0)
            {
                values[i] = analyze_expression(*ast.members[j].value, arena, symbols, prototype);

                if (!coerce_value(&values[i], type.struct_type.members[i].type))
                {
                    fprintf(stderr, "Cannot use expression of incorrect type for member %s of struct %s in struct literal\n", ast.members[j].name, type.struct_type.name);
                }

                break;
            }
        }
    }

    return (IrExpression) {
        .kind = IStructLiteral,
        .struct_literal = (IrStructLiteral) {
            .type = type,
            .members = values,
            .members_length = ast.members_length,
        },
        .type = type,
    };
}

static IrExpression analyze_index(AstIndex ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *array = arena_alloc(arena, sizeof(IrExpression));
    *array = analyze_expression(*ast.array, arena, symbols, prototype);

    if (array->type.kind != IArray && array->type.kind != ISlice)
    {
        fprintf(stderr, "Cannot index non-array types\n");
        exit(1);
    }

    IrExpression *index = arena_alloc(arena, sizeof(IrExpression));
    *index = analyze_expression(*ast.index, arena, symbols, prototype);

    if (!coerce_value(index, get_numeric_type(IUint, 8)))
    {
        fprintf(stderr, "Can only index arrays with a U64\n");
        exit(1);
    }

    return (IrExpression) {
        .kind = IIndex,
        .index = (IrIndex) {
            .array = array,
            .index = index,
        },
        .type = *array->type.array.inner,
    };
}

static IrExpression analyze_slice_index(AstSliceIndex ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *array = arena_alloc(arena, sizeof(IrExpression));
    *array = analyze_expression(*ast.array, arena, symbols, prototype);

    IrExpression *start = arena_alloc(arena, sizeof(IrExpression));
    *start = analyze_expression(*ast.start, arena, symbols, prototype);

    IrExpression *end = arena_alloc(arena, sizeof(IrExpression));
    *end = analyze_expression(*ast.end, arena, symbols, prototype);

    if (array->type.kind != IArray && array->type.kind != ISlice)
    {
        fprintf(stderr, "Can only slice arrays\n");
        exit(1);
    }

    if (!coerce_value(start, get_numeric_type(IUint, 8)) || !coerce_value(end, get_numeric_type(IUint, 8)))
    {
        fprintf(stderr, "Can only use U64 to slice array\n");
        exit(1);
    }

    return (IrExpression) {
        .kind = ISliceIndex,
        .slice_index = (IrSliceIndex) {
            .array = array,
            .start = start,
            .end = end,
        },
        .type = get_slice_type(*array->type.inner, arena),
    };
}

static IrExpression analyze_dot(AstDot ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression *parent = arena_alloc(arena, sizeof(IrExpression));
    *parent = analyze_expression(*ast.parent, arena, symbols, prototype);

    IrType type = parent->type;

    if (type.kind == IStruct)
    {
        int member_index = 0;
        bool found = false;

        for (size_t i = 0; i < type.struct_type.members_length; i++)
        {
            if (strcmp(type.struct_type.members[i].name, ast.member) == 0)
            {
                member_index = i;
                found = true;
                break;
            }
        }

        if (!found)
        {
            fprintf(stderr, "Struct type '%s' has no member named '%s'\n", type.struct_type.name, ast.member);
            exit(1);
        }

        return (IrExpression) {
            .kind = IStructMemberAccess,
            .struct_member_access = (IrStructMemberAccess) {
                .parent = parent,
                .member_index = member_index,
            },
            .type = type.struct_type.members[member_index].type,
        };
    }

    if (type.kind == IArray && strcmp(ast.member, "len") == 0)
    {
        return (IrExpression) {
            .kind = IIntLiteral,
            .int_literal = type.array.length,
            .type = get_numeric_type(IUint, 8),
        };
    }

    if (type.kind == ISlice && strcmp(ast.member, "len") == 0)
    {
        return (IrExpression) {
            .kind = ISliceLength,
            .slice_length = parent,
            .type = get_numeric_type(IUint, 8),
        };
    }

    fprintf(stderr, "Invalid type for dot operator.\n");
    exit(1);

    return (IrExpression) {};
}

static IrExpression analyze_expression(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    switch (ast.kind)
    {
        case AIntLiteral:
            return (IrExpression) {
                .kind = IIntLiteral,
                .int_literal = ast.int_literal,
                .type = get_basic_type(IUntypedInt),
            };

        case AFloatLiteral:
            return (IrExpression) {
                .kind = IFloatLiteral,
                .float_literal = ast.float_literal,
                .type = get_numeric_type(IFloat, 4),
            };

        case ACharacterLiteral:
            return (IrExpression) {
                .kind = ICharacterLiteral,
                .character_literal = ast.character_literal,
                .type = get_numeric_type(IUint, 1),
            };

        case AFunctionCall:
            return analyze_function_call(ast.function_call, arena, symbols);

        case AVariable:
            return analyze_variable_call(ast.variable, symbols);

        case ABinary:
            return analyze_binary(ast.binary, arena, symbols, prototype);

        case ANegate:
            return analyze_negate(*ast.unary, arena, symbols, prototype);

        case AReference:
            return analyze_reference(*ast.unary, arena, symbols, prototype);

        case ADereference:
            return analyze_dereference(*ast.unary, arena, symbols, prototype);

        case ABlock:
            return analyze_block_expression(ast.block, arena, symbols, prototype);

        case AIfExpression:
            return analyze_if_expression(*ast.if_expression, arena, symbols, prototype);

        case ASwitchExpression:
            return analyze_switch_expression(*ast.switch_expression, arena, symbols, prototype);

        case ACast:
            return analyze_cast(ast.cast, arena, symbols, prototype);

        case AArrayLiteral:
            return analyze_array_literal(ast.array_literal, arena, symbols, prototype);

        case AStructLiteral:
            return analyze_struct_literal(ast.struct_literal, arena, symbols, prototype);

        case AIndex:
            return analyze_index(ast.index, arena, symbols, prototype);

        case ASliceIndex:
            return analyze_slice_index(ast.slice_index, arena, symbols, prototype);

        case ADot:
            return analyze_dot(ast.dot, arena, symbols, prototype);
    }

    return (IrExpression) {};
}

static IrVariableDefinition analyze_variable_definition(AstVariableDefinition ast, Arena *arena, Arena *temp_arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression value = analyze_expression(ast.value, arena, symbols, prototype);

    if (value.type.kind == IArray && value.kind != IArrayLiteral)
    {
        fprintf(stderr, "Cannot assign from one array to another\n");
        exit(1);
    }

    if (!ast.infrenced && !coerce_value(&value, analyze_type(ast.type, arena, symbols)))
    {
        fprintf(stderr, "Cannot assign value of different type to variable '%s'\n", ast.name);
        exit(1);
    }

    make_typed(&value);

    if (value.type.kind == IVoid)
    {
        fprintf(stderr, "Cannot make a variable of type void\n");
        exit(1);
    }

    IrVariable *variable = arena_alloc(arena, sizeof(IrVariable));
    *variable = (IrVariable) {
        .name = move_string(ast.name, arena),
        .address = NULL,
        .type = value.type,
    };

    Symbol *symbol = arena_alloc(temp_arena, sizeof(Symbol));
    *symbol = (Symbol) {
        .kind = SymLocal,
        .local = variable,
    };

    if (!hash_map_insert(symbols, variable->name, symbol))
    {
        fprintf(stderr, "Cannot redefine variable '%s'\n", variable->name);
        exit(1);
    }

    return (IrVariableDefinition) {
        .variable = variable,
        .value = value,
    };
}

static IrAssignment analyze_assignment(AstAssignment ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression left_hand_side = analyze_expression(ast.left_hand_side, arena, symbols, prototype);

    IrExpression value = analyze_expression(ast.value, arena, symbols, prototype);

    if (value.type.kind == IArray && value.kind != IArrayLiteral)
    {
        fprintf(stderr, "Cannot assign from one array to another\n");
        exit(1);
    }

    switch (left_hand_side.kind)
    {
        case IVariable:
            if (!coerce_value(&value, left_hand_side.variable->type))
            {
                fprintf(stderr, "Cannot assign value of different type to variable '%s'\n", ast.left_hand_side.variable);
                exit(1);
            }
            break;

        case IDereference:
            if (!coerce_value(&value, left_hand_side.type))
            {
                fprintf(stderr, "Cannot assign value of wrong type to address\n");
                exit(1);
            }
            break;

        case IIndex:
            if (!coerce_value(&value, left_hand_side.type))
            {
                fprintf(stderr, "Cannot assign value of wrong type to array element\n");
                exit(1);
            }
            break;

        case IStructMemberAccess:
            if (!coerce_value(&value, left_hand_side.struct_member_access.parent->type.struct_type.members[left_hand_side.struct_member_access.member_index].type))
            {
                fprintf(stderr, "Cannot assign value of wrong type to member '%s' of struct of type '%s'\n", left_hand_side.struct_member_access.parent->type.struct_type.members[left_hand_side.struct_member_access.member_index].name, left_hand_side.struct_member_access.parent->type.struct_type.name);
                exit(1);
            }
            break;

        default:
            fprintf(stderr, "Invalid left hand side of assignment\n");
            exit(1);
            break;
    }

    return (IrAssignment) {
        .left_hand_side = left_hand_side,
        .value = value,
    };
}

static IrReturn analyze_return(AstReturn ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    if (ast.has_value)
    {
        if (prototype.return_type.kind == IVoid)
        {
            fprintf(stderr, "Cannot return anything from void function '%s'\n", prototype.name);
            exit(1);
        }

        IrExpression value = analyze_expression(ast.value, arena, symbols, prototype);

        if (!coerce_value(&value, prototype.return_type))
        {
            fprintf(stderr, "Type of value returned from function '%s' does not match it's specified return type\n", prototype.name);
            exit(1);
        }

        return (IrReturn) {
            .has_value = true,
            .value = value,
        };
    }
    else
    {
        if (prototype.return_type.kind != IVoid)
        {
            fprintf(stderr, "Cannot return something from void function '%s'\n", prototype.name);
            exit(1);
        }

        return (IrReturn) {
            .has_value = false,
        };
    }
}

static IrYield analyze_yield(AstYield ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression value = analyze_expression(ast.value, arena, symbols, prototype);
    make_typed(&value);

    return (IrYield) {
        .value = value,
    };
}

static IrWhile analyze_while(AstWhile ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    return (IrWhile) {
        .condition = analyze_expression(ast.condition, arena, symbols, prototype),
        .body = analyze_block(ast.body, arena, symbols, prototype),
    };
}

static IrExpression analyze_expression_statement(AstExpression ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrExpression expression = analyze_expression(ast, arena, symbols, prototype);

    if (expression.type.kind != IVoid)
    {
        fprintf(stderr, "Unused value in expression statement\n");
        exit(1);
    }

    return expression;
}

static IrBlock analyze_sub_block(AstBlock ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrBlock block = analyze_block(ast, arena, symbols, prototype);

    if (block.yield_type.kind != IVoid)
    {
        fprintf(stderr, "Unused yielded value from block\n");
        exit(1);
    }

    return block;
}

static IrIf analyze_if_statement(AstIf ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    IrIf if_statement = analyze_if(ast, arena, symbols, prototype);

    if (if_statement.yield_type.kind != IVoid)
    {
        fprintf(stderr, "Cannot yield value from if statement\n");
        exit(1);
    }

    return if_statement;
}

static IrStatement analyze_statement(AstStatement ast, Arena *arena, Arena *temp_arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    switch (ast.kind)
    {
        case AVariableDefinition:
            return (IrStatement) {
                .kind = IVariableDefinition,
                .variable_definition = analyze_variable_definition(ast.variable_definition, arena, temp_arena, symbols, prototype),
            };

        case AAssignment:
            return (IrStatement) {
                .kind = IAssignment,
                .assignment = analyze_assignment(ast.assignment, arena, symbols, prototype),
            };

        case AReturn:
            return (IrStatement) {
                .kind = IReturn,
                .return_statement = analyze_return(ast.return_statement, arena, symbols, prototype),
            };

        case AYield:
            return (IrStatement) {
                .kind = IYield,
                .yield = analyze_yield(ast.yield, arena, symbols, prototype),
            };

        case AIfStatement:
            return (IrStatement) {
                .kind = IIfStatement,
                .if_statement = analyze_if_statement(ast.if_statement, arena, symbols, prototype),
            };

        case ASwitchStatement:
            return (IrStatement) {
                .kind = ISwitchStatement,
                .switch_statement = analyze_switch(ast.switch_statement, arena, symbols, prototype),
            };

        case AWhile:
            return (IrStatement) {
                .kind = IWhile,
                .while_statement = analyze_while(ast.while_statement, arena, symbols, prototype),
            };

        case AExpression:
            return (IrStatement) {
                .kind = IExpression,
                .expression = analyze_expression_statement(ast.expression, arena, symbols, prototype),
            };

        case ASubBlock:
            return (IrStatement) {
                .kind = ISubBlock,
                .sub_block = analyze_sub_block(ast.sub_block, arena, symbols, prototype),
            };
    }

    return (IrStatement) {};
}

static IrBlock analyze_block(AstBlock ast, Arena *arena, HashMap *symbols, IrFunctionPrototype prototype)
{
    ArenaScratch scratch = arena_scratch_get();
    symbols = hash_map_copy(scratch.arena, symbols);

    IrStatement *statements = arena_alloc(arena, ast.statements_length * sizeof(IrStatement));
    bool returns = false;

    IrType yield_type = get_basic_type(IVoid);

    for (size_t i = 0; i < ast.statements_length; i++)
    {
        statements[i] = analyze_statement(ast.statements[i], arena, scratch.arena, symbols, prototype);

        if (statement_terminates(statements[i]))
        {
            if (i < ast.statements_length - 1)
            {
                fprintf(stderr, "Unreachable code\n");
                exit(1);
            }

            if (statement_returns(statements[i]))
            {
                returns = true;
            }

            if (statements[i].kind == IYield)
            {
                yield_type = statements[i].yield.value.type;
            }

            break;
        }
    }

    arena_scratch_release(scratch);

    return (IrBlock) {
        .statements = statements,
        .statements_length = ast.statements_length,
        .yield_type = yield_type,
        .returns = returns,
    };
}

static IrFunctionPrototype analyze_function_prototype(AstFunctionDefinition ast, Arena *arena, HashMap *symbols)
{
    IrParameter *parameters = arena_alloc(arena, ast.parameters_length * sizeof(IrParameter));

    for (size_t i = 0; i < ast.parameters_length; i++)
    {
        parameters[i] = (IrParameter) {
            .name = move_string(ast.parameters[i].name, arena),
            .type = analyze_type(ast.parameters[i].type, arena, symbols),
            .value = NULL,
        };
    }

    return (IrFunctionPrototype) {
        .name = move_string(ast.name, arena),
        .parameters = parameters,
        .parameters_length = ast.parameters_length,
        .return_type = analyze_type(ast.return_type, arena, symbols),
        .signature = NULL,
        .address = NULL,
    };
}

static IrFunctionDefinition analyze_function_definition(AstFunctionDefinition ast, Arena *arena, HashMap *symbols, IrFunctionPrototype *prototype)
{
    ArenaScratch scratch = arena_scratch_get();
    symbols = hash_map_copy(scratch.arena, symbols);

    for (size_t i = 0; i < ast.parameters_length; i++)
    {
        Symbol *symbol = arena_alloc(scratch.arena, sizeof(Symbol));
        *symbol = (Symbol) {
            .kind = SymParameter,
            .parameter = &prototype->parameters[i],
        };

        if (!hash_map_insert(symbols, ast.parameters[i].name, symbol))
        {
            symbol = hash_map_lookup(symbols, ast.parameters[i].name);

            if (symbol->kind == SymParameter)
            {
                fprintf(stderr, "Repeat of parameter '%s' in function '%s'\n", ast.parameters[i].name, ast.name);
            }
            else
            {
                fprintf(stderr, "Parameter '%s' in function '%s' shadows previously defined symbol\n", ast.parameters[i].name, ast.name);
            }

            exit(1);
        }
    }

    IrBlock body = analyze_block(ast.body, arena, symbols, *prototype);

    if (ast.body.statements == NULL)
    {
        body.statements = NULL;
    }

    arena_scratch_release(scratch);

    if (body.statements != NULL && prototype->return_type.kind != IVoid && !body.returns)
    {
        fprintf(stderr, "Must return from non-void function '%s'\n", ast.name);
        exit(1);
    }

    return (IrFunctionDefinition) {
        .prototype = prototype,
        .body = body,
    };
}

static IrType analyze_struct(AstStructDefinition ast, Arena *arena, HashMap *symbols)
{
    IrStructMember *members = arena_alloc(arena, ast.members_length * sizeof(IrStructMember));

    for (size_t i = 0; i < ast.members_length; i++)
    {
        members[i] = (IrStructMember) {
            .name = ast.members[i].name,
            .type = analyze_type(ast.members[i].type, arena, symbols),
        };
    }

    return (IrType) {
        .kind = IStruct,
        .struct_type = (IrStruct) {
            .name = ast.name,
            .members = members,
            .members_length = ast.members_length,
        },
    };
}

Ir *semantic_analysis_analyze(Ast *ast, Arena *arena)
{
    ArenaScratch scratch = arena_scratch_get();
    HashMap *symbols = hash_map_create(scratch.arena);

    IrType types[] = {
        get_basic_type(IVoid), get_basic_type(IBool),
        get_numeric_type(IInt, 1), get_numeric_type(IInt, 2), get_numeric_type(IInt, 4), get_numeric_type(IInt, 8),
        get_numeric_type(IUint, 1), get_numeric_type(IUint, 2), get_numeric_type(IUint, 4), get_numeric_type(IUint, 8),
        get_numeric_type(IFloat, 4), get_numeric_type(IFloat, 8),
    };
    char *type_names[] = {
        "Void", "Bool",
        "I8", "I16", "I32", "I64",
        "U8", "U16", "U32", "U64",
        "F32", "F64",
    };

    for (size_t i = 0; i < sizeof(types) / sizeof(IrType); i++)
    {
        Symbol *symbol = arena_alloc(scratch.arena, sizeof(Symbol));
        *symbol = (Symbol) {
            .kind = SymType,
            .type = types[i],
        };

        hash_map_insert(symbols, type_names[i], symbol);
    }

    for (size_t i = 0; i < ast->structs_length; i++)
    {
        IrType type = analyze_struct(ast->structs[i], arena, symbols);

        Symbol *symbol = arena_alloc(scratch.arena, sizeof(Symbol));
        *symbol = (Symbol) {
            .kind = SymType,
            .type = type,
        };

        if (!hash_map_insert(symbols, type.struct_type.name, symbol))
        {
            fprintf(stderr, "Cannot redefine struct '%s'\n", type.struct_type.name);
            exit(1);
        }
    }

    IrFunctionPrototype *prototypes = arena_alloc(arena, ast->functions_length * sizeof(IrFunctionPrototype));

    for (size_t i = 0; i < ast->functions_length; i++)
    {
        prototypes[i] = analyze_function_prototype(ast->functions[i], arena, symbols);

        Symbol *symbol = arena_alloc(scratch.arena, sizeof(Symbol));
        *symbol = (Symbol) {
            .kind = SymFunction,
            .function = &prototypes[i],
        };

        if (!hash_map_insert(symbols, prototypes[i].name, symbol))
        {
            fprintf(stderr, "Cannot redefine function '%s'\n", prototypes[i].name);
            exit(1);
        }
    }

    IrFunctionDefinition *functions = arena_alloc(arena, ast->functions_length * sizeof(IrFunctionDefinition));

    for (size_t i = 0; i < ast->functions_length; i++)
    {
        functions[i] = analyze_function_definition(ast->functions[i], arena, symbols, &prototypes[i]);
    }

    arena_scratch_release(scratch);

    Ir *ir = arena_alloc(arena, sizeof(Ir));
    *ir = (Ir) {
        .functions = functions,
        .functions_length = ast->functions_length,
    };

    return ir;
}
