#ifndef HASHMAP_H
#define HASHMAP_H

#include <stdbool.h>

#include "arena.h"

typedef struct HashMap HashMap;

HashMap *hash_map_create(Arena *arena);
bool hash_map_insert(HashMap *hash_map, const char *key, void *value);
void *hash_map_lookup(HashMap *hash_map, const char *key);
HashMap *hash_map_copy(Arena *arena, HashMap *original);

#endif
